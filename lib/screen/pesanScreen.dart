import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:elenpa/model/pesan.dart';
import 'package:loadmore/loadmore.dart';
import 'dart:async';

class PesanScreen extends StatefulWidget {
  PesanScreen() : super();

  final String title = "Inbox";

  @override
  PesanScreenState createState() => PesanScreenState();
}

class PesanScreenState extends State<PesanScreen> {
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  List pesan=[];
  List selectedPesan;
  bool sort;

  int total = 0;
  int start = 0;
  int length = 10;
  int get count => pesan.length;
  Future<void> getPesan() async{
    show();
    NetworkUtil _netUtil = new NetworkUtil();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"start": start.toString(), "length":length.toString()};
    _netUtil.post(NetworkUtil.MESSAGE_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get data");
        dismiss();
        return null;
      } else {
        if(res['data'].length>0){
        setState(() {
          start += 10;
          total = res['data'][0]['total_row'];
          pesan += res['data'].map((model) => Pesan.fromJson(model)).toList();
//          print(res['data'][0]);
        });
        }
        dismiss();
      }
    });
  }
  @override
  void initState() {
    sort = false;
    selectedPesan = [];
    getPesan();
    super.initState();
  }

  onSortColum(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        pesan.sort((a, b) => a.nama_lengkap.compareTo(b.nama_lengkap));
      } else {
        pesan.sort((a, b) => b.nama_lengkap.compareTo(a.nama_lengkap));
      }
    }
  }

  Future<void> writeMessage(String id_user, String user_name) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('id_user_receiver', id_user);
    prefs.setString('user_name_receiver', user_name);
    Navigator.pushNamed(
        context, '/ChatScreen'
    );
  }


  @override
  Widget build(BuildContext context) {

    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );
    return Scaffold(
      appBar: topAppBar,
      body: (_isShowing? Center(child: CircularProgressIndicator()):pesan.length>0?Container(
        child: RefreshIndicator(
          child: LoadMore(
            isFinish: count >= total,
            onLoadMore: _loadMore,
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0.0)),
                    elevation: 0.0,
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: (pesan[index].foto_lawan!=""?Image.network(NetworkUtil.ASSETS_FILE_URL+pesan[index].foto_lawan):Image.asset("assets/images/user.png")),
                            title: Text(pesan[index].nama_lawan,style: TextStyle(fontWeight: FontWeight.bold)),
                            subtitle: Text('${pesan[index].isi_pesan}\n${pesan[index].tanggal_kirim}'),
                            contentPadding: EdgeInsets.all(0.0),
                            dense: false,
                            isThreeLine: true,
                            trailing: Icon(FontAwesomeIcons.caretRight),
                            onTap:(){
                              writeMessage(pesan[index].penerima,pesan[index].nama_lawan);
                            },
                          )
                        ]));
              },
              itemCount: count,
            ),
            whenEmptyLoad: false,
            delegate: DefaultLoadMoreDelegate(),
            textBuilder: DefaultLoadMoreTextBuilder.english,
          ),
          onRefresh: _refresh,
        ),
      ):Center(child: Text("Tidak ada data"))),

      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.of(context).pushNamed('/NewMessageScreen');
        },
        label: Text('PESAN BARU'),
        icon: Icon(Icons.add),
        backgroundColor: Colors.green,
      ),
    );
  }
  Future<bool> _loadMore() async {
    print("onLoadMore");
    await Future.delayed(Duration(seconds: 0, milliseconds: 100));
    getPesan();
    return true;
  }

  Future<void> _refresh() async {
    print("refresh");
    await Future.delayed(Duration(seconds: 0, milliseconds: 2000));
    pesan.clear();
    setState(() {
      start = 0;
      length = 10;
    });
    getPesan();
  }
}

