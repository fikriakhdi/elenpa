import 'package:elenpa/model/perusahaan.dart';
import 'package:elenpa/utils/network_util.dart';
import 'package:flutter/material.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:shared_preferences/shared_preferences.dart';
class DetailPerusahaanScreen extends StatefulWidget {
//  ProfileScreen({Key key, this.title}) : super(key: key);

  final String title ="Detail Perusahaan";

  @override
  _DetailPerusahaanScreenState createState() => new _DetailPerusahaanScreenState();
}
final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
class _DetailPerusahaanScreenState extends State<DetailPerusahaanScreen> {
  var  perusahaan = {};
  NetworkUtil _netUtil = new NetworkUtil();
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  Future<void> getProfileData() async{

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    String user_id = prefs.getString('edit_perusahaan');
    print("USER ID NYA");
    print(user_id);
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"user_id": user_id};
    return _netUtil.post(NetworkUtil.SINGLE_USER, headers:headers, body:body).then((
        dynamic res) async {
      if (res['status'] == 400) {
        print("Error to get data");
        dismiss();
      } else{
        setState(() {
          perusahaan =(res['data']);
          
        dismiss();
        });
        
      }
    });
  }
  @override
  void initState() {
    show();
    getProfileData();
//  downloads = getLessons();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final levelIndicator = Container(
      child: Container(
      ),
    );




    return Scaffold(

      body: (_isShowing? Center(child: CircularProgressIndicator()):Column(
        children: <Widget>[Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.4,
          padding: EdgeInsets.all(40.0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, .9)),
          child: Center(
            child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.info,
          color: Colors.white,
          size: 40.0,
        ),
        Container(
          width: 70.0,
          child: new Divider(color: Colors.white),
        ),
        SizedBox(height: 10.0),
        Text(
          "Detail Perusahaan",
          style: TextStyle(color: Colors.white, fontSize: 20.0),
        ),
        SizedBox(height: 30.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
                flex: 10,
                child: Padding(
                    padding: EdgeInsets.only(left: 0.0),
                   child: Text(
                     perusahaan['nama_lengkap'],
                    //  perusahaan.nama_perusahaan,
                     style: TextStyle(color: Colors.white, fontSize: 23.0),
                   )
                )),
          ],
        ),
      ],
    ),
          ),
        ),
        Positioned(
          left: 8.0,
          top: 60.0,
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back, color: Colors.white),
          ),
        )
      ],
    ),Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(10.0),
      child: Container(
        child: Column(
          children: <Widget>[
            Text("Nama Perusahaan :"),
            Text(perusahaan['nama_lengkap'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),),
            SizedBox(height: 5.0),Text("Telepon :"),
            Text(perusahaan['nomor_telp'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold)),
            SizedBox(height: 5.0),Text("Email :"),
            Text(perusahaan['email'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),),
            SizedBox(height: 5.0),Text("Alamat :"),
            Text(perusahaan['alamat'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),),
            SizedBox(height: 5.0),Text("Provinsi :"),
            Text(perusahaan['nama_provinsi'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),),
            SizedBox(height: 5.0),Text("Kota :"),
            Text(perusahaan['nama_kota'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),), 
            Container(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        width: MediaQuery.of(context).size.width,
        child: RaisedButton(
          onPressed: () => {
            editCompany(perusahaan['user_id'])
          },
          color: Color.fromRGBO(58, 66, 86, 1.0),
          child:
          Text("Ubah Data", style: TextStyle(color: Colors.white)),
        ))],
        ),
      ),
    )],
      )),
    );
  }
  Future<void> editCompany(String id_perusahaan) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('edit_perusahaan', id_perusahaan);
    Navigator.pushNamed(
        context, '/editPerusahaanScreen'
    );
  }
}
