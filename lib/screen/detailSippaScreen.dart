import 'package:elenpa/model/sippa.dart';
import 'package:elenpa/utils/network_util.dart';
import 'package:flutter/material.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:shared_preferences/shared_preferences.dart';
class DetailSippaScreen extends StatefulWidget {
//  ProfileScreen({Key key, this.title}) : super(key: key);

  final String title ="Detail Sippa";

  @override
  _DetailSippaScreenState createState() => new _DetailSippaScreenState();
}
final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
class _DetailSippaScreenState extends State<DetailSippaScreen> {
  var  sippa = {};
  NetworkUtil _netUtil = new NetworkUtil();
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  Future<void> getSippaData() async{

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    String id = prefs.getString('edit_sippa');
    print("ID NYA");
    print(id);
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"intake_id": id};
    return _netUtil.post(NetworkUtil.SIPPA_SINGLE_URL, headers:headers, body:body).then((
        dynamic res) async {
      if (res['status'] == 400) {
        print("Error to get data");
        dismiss();
      } else{
        setState(() {
          sippa =(res['data']);
        dismiss();
        });
        
      }
    });
  }
  @override
  void initState() {
    show();
    getSippaData();
//  downloads = getLessons();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {




    return Scaffold(

      body: (_isShowing? Center(child: CircularProgressIndicator()):Column(
        children: <Widget>[Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.4,
          padding: EdgeInsets.all(40.0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, .9)),
          child: Center(
            child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.info,
          color: Colors.white,
          size: 40.0,
        ),
        Container(
          width: 70.0,
          child: new Divider(color: Colors.white),
        ),
        SizedBox(height: 10.0),
        Text(
          "Detail Sippa",
          style: TextStyle(color: Colors.white, fontSize: 20.0),
        ),
        SizedBox(height: 30.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
                flex: 10,
                child: Padding(
                    padding: EdgeInsets.only(left: 0.0),
                   child: Text(
                     sippa['nomor_sippa'],
                    //  sippa.nama_sippa,
                     style: TextStyle(color: Colors.white, fontSize: 23.0),
                   )
                )),
          ],
        ),
      ],
    ),
          ),
        ),
        Positioned(
          left: 8.0,
          top: 60.0,
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back, color: Colors.white),
          ),
        )
      ],
    ),Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(10.0),
      child: Container(
        child: Column(
          children: <Widget>[
            Text("Nomor Sippa :"),
            Text(sippa['nomor_sippa'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),),
            SizedBox(height: 5.0),Text("Sumber Mata Air :"),
            Text(sippa['sumber_mata_air'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold)),
            SizedBox(height: 5.0),Text("Tanggal Penetapan :"),
            Text(sippa['tanggal_penetapan'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),),
            SizedBox(height: 5.0),Text("Tanggal Habis :"),
            Text(sippa['tanggal_habis'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),),
            SizedBox(height: 5.0),Text("Water Meter :"),
            Text((sippa['water_meter']=="0"?"TIDAK ADA":"ADA"),style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),),
            SizedBox(height: 5.0),Text("Maks Debit :"),
            Text(sippa['max_debit'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),)],
        ),
      ),
    )],
      )),
    );
  }
}
