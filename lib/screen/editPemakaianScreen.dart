
import 'package:flutter/material.dart';
import 'package:elenpa/theme/theme.dart';
import 'dart:async';
import 'package:elenpa/utils/network_util.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:intl/intl.dart';

class EditPemakaianScreen extends StatefulWidget {
//  ProfileScreen({Key key, this.title}) : super(key: key);

  final String title ="Edit Pemakaian";

  @override
  _EditPemakaianScreenState createState() => new _EditPemakaianScreenState();
}
final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
class _EditPemakaianScreenState extends State<EditPemakaianScreen> {
  NetworkUtil _netUtil = new NetworkUtil();
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  TextEditingController tanggal_pemakaian = new TextEditingController();
  TextEditingController volume_meter = new TextEditingController();
  int _selectedCompany = null;
  int _selectedIntake = null;
  File _image1,_image2;
  var formatter = new DateFormat('yyyy-MM-dd');
  dynamic Pemakaian = {};
  List<DropdownMenuItem<int>> companyList = [];
  List<DropdownMenuItem<int>> intakeList = [];
  Future cameraVolume() async {
      var image = await ImagePicker.pickImage(
        source: ImageSource.camera
      );

      setState(() {
        _image1 = image;
      });
    }
  Future cameraBeritaAcara() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.camera
    );

    setState(() {
      _image2 = image;
    });
  }
  Future<void> loadPerusahaanList() async{
    show();
    companyList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    return _netUtil.get(NetworkUtil.COMPANY_URL,headers).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get data");
        dismiss();
        return null;
      } else {
        setState(() {
          print("datanya");
          print(res);
          res['data'].map((model) => companyList.add(new DropdownMenuItem(
            child: new Text(model['nama_perusahaan']),
            value: int.parse((model['id_perusahaan']!=""?model['id_perusahaan']:0)),
          ))).toList();
          print(companyList);
        });
        dismiss();
      }
    });
  }

  Future<void> loadIntakeList(String perusahaan_id) async{
//    show();
    intakeList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"perusahaan_id": perusahaan_id};
    return _netUtil.post(NetworkUtil.INTAKE_COMPANY_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get data");
        dismiss();
        return null;
      } else {
        if(res['data'].length>0){
          print("kesini");
        setState(() {
          print(res['data']);
          res['data'].map((model) => intakeList.add(new DropdownMenuItem(
            child: new Text(model['sumber_mata_air']),
            value: int.parse((model['intake_id']!=null?model['intake_id']:0)),
          ))).toList();
          _selectedIntake = int.parse(res['data'][0]['intake_id']);
        });
        } else _selectedIntake = null;
      }
    });
  }

  Future<bool> updateUsage(String intake_id,perusahaan_id,photo_meteran, upload_berita_acara, volume_meter, tanggal_pemakaian) async{
    intakeList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    String edit_pemakaian = prefs.getString('edit_pemakaian');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"intake_id":intake_id,"perusahaan_id":perusahaan_id,"pemakaian_id":edit_pemakaian, "volume_meter":volume_meter, "tanggal_pemakaian":tanggal_pemakaian};
    if(photo_meteran!="") body["photo_meteran"]=  photo_meteran;
    if(upload_berita_acara!="") body["upload_berita_acara"]=upload_berita_acara;
    print(body);
    return _netUtil.post(NetworkUtil.UPDATE_USAGE_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to post data");
        dismiss();
        return null;
      } else {
        print("lolos");
;        getPemakaianData();
        dismiss();
        return true;
      }
    });
  }

  Future<bool> setIntakeList(String perusahaan_id) async{
    show();
    intakeList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"perusahaan_id": perusahaan_id};
    return _netUtil.post(NetworkUtil.INTAKE_COMPANY_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        dismiss();
        return false;
      } else {
        dismiss();
        if(res['data'].length>0){
        setState(() {
          res['data'].map((model) => intakeList.add(new DropdownMenuItem(
            child: new Text(model['sumber_mata_air']),
            value: int.parse((model['intake_id']!=null?model['intake_id']:0)),
          ))).toList();
          _selectedIntake = int.parse(res['data'][0]['intake_id']);
        });
        return true;
        } else {
          _selectedIntake = null;
          return false;
        }
      }
    });
  }
  Future<bool> signout() async{
    show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    return _netUtil.get(NetworkUtil.LOGOUT_URL,headers).then((dynamic res) async{
      if(res['status']==400) {
        return false;
      }
      else {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("userId", "");
        prefs.setString("P3NPA_PIN", "");
        dismiss();
        return true;
      }
    });
  }

    Future<void> selectDate()async{     
      var date = DateTime.now(); 
        final DateTime d = await showDatePicker(
          
          firstDate:new DateTime(date.year, date.month - 12, date.day),
          initialDate: new DateFormat("yyyy-MM-dd").parse(Pemakaian['tanggal']),
          lastDate: DateTime.now(),
            context: context,
            );
            
    if (d != null) //if the user has selected a date
      setState(() {
        // we format the selected date and assign it to the state variable
        tanggal_pemakaian.text = formatter.format(d);
      });
    }

  Future<void> getPemakaianData() async{

    show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    String pemakaian_id = prefs.getString('edit_pemakaian');
    print("ID NYA");
    print(pemakaian_id);
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"pemakaian_id": pemakaian_id};
    return _netUtil.post(NetworkUtil.USAGE_SINGLE_URL, headers:headers, body:body).then((
        dynamic res) async {
      if (res['status'] == 400) {
        print("Error to get data");
        dismiss();
      } else{
        print("mantab");
        setState(() {
          Pemakaian = res['data'];
        // print(Pemakaian['id']);
          volume_meter.text = Pemakaian['volume_meter'];
          tanggal_pemakaian.text = Pemakaian['tanggal'];
          _selectedCompany = int.parse(Pemakaian['perusahaan_id']);
          setIntakeList(Pemakaian['perusahaan_id']).then((ret){
            print("WOOOOOOOOOY");
            _selectedIntake = int.parse(Pemakaian['intake_id']);
          });
        });
        
        dismiss();
      }
    });
  }
  @override
  void initState() {
    show();
    var now = new DateTime.now();
    tanggal_pemakaian.text = formatter.format(now);
    getPemakaianData();
    loadPerusahaanList();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {



    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );
    return new Scaffold(
        key: _scaffoldState,
        appBar: topAppBar,
        body: (_isShowing? Center(child: CircularProgressIndicator()):new ListView(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 10.0),
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: DropdownButton(
                      hint: new Text('Pilih Perusahaan'),
                      items: companyList,
                      value: _selectedCompany,
                      onChanged: (value) {
                        setState(() {
                          _selectedCompany = value;
                          loadIntakeList(value.toString());
                        });
                      },
                      isExpanded: true,
                    )),
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: DropdownButton(
                        hint: new Text('Pilih Intake'),
                        items: intakeList,
                        value: _selectedIntake,
                        onChanged: (value) {
                          setState(() {
                            _selectedIntake = value;
                          });
                        },
                        isExpanded: true,
                      ),

                    ),
                    TextFormField(
                      controller:volume_meter,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Tolong Masukkan Volume Meter';
                        }
                        return null;
                      },

                      decoration: new InputDecoration(hintText: "Volume Meter", contentPadding: const EdgeInsets.all(20.0)),
                    ),
                    TextFormField(
                      controller:tanggal_pemakaian,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Tolong Masukkan Tanggal pemakaian';
                        }
                        return null;
                      },
                    onTap:(){
                      selectDate();
                    },
                      decoration: new InputDecoration(hintText: "Tanggal Pemakaian", contentPadding: const EdgeInsets.all(20.0)),
                    ),
                    SizedBox(height: 15.0),
                    Text("Volume Meter :"),
                    Container(
                        height: 200.0,
                        width: 300.0,
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.grey,
                          elevation: 7.0,
                          child: GestureDetector(
                            onTap: () {
                              cameraVolume();
                            },
                            child: _image1!=null?new Image.file(_image1):Image.asset('assets/images/noimage.jpg'),
                          ),
                        )),
                    SizedBox(height: 15.0),
                    Text("Berita Acara :"),
                    Container(
                        height: 200.0,
                        width: 300.0,
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.grey,
                          elevation: 7.0,
                          child: GestureDetector(
                            onTap: () {
                              cameraBeritaAcara();
                            },
                            child: _image2!=null?new Image.file(_image2):Image.asset('assets/images/noimage.jpg'),
                          ),
                        )),
                    SizedBox(height: 15.0),
                    Container(
                        height: 45.0,
                        width: 115.0,
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          shadowColor: Colors.greenAccent,
                          color: Colors.green,
                          elevation: 7.0,
                          child: GestureDetector(
                            onTap: () {
                              show();
                              String fotoVolume = (_image1!=null?'data:image/png;base64,' +base64Encode(_image1.readAsBytesSync()):"");
                              String fotoBeritaAcara = (_image2!=null?'data:image/png;base64,' +base64Encode(_image2.readAsBytesSync()):"");
                              updateUsage(_selectedIntake.toString(),_selectedCompany.toString(), fotoVolume,fotoBeritaAcara,volume_meter.text, tanggal_pemakaian.text).then((isSuccess) {
                                if(isSuccess){
                              _scaffoldState.currentState.showSnackBar(
                                SnackBar(
                                  content: Text("Edit Sukses"),
                                ),
                              );
                              } else{
                              _scaffoldState.currentState.showSnackBar(
                                SnackBar(
                                  content: Text("Edit Gagal"),
                                ),
                              );
                              }
                            });
                            },
                            child: Center(
                              child: Text(
                                'SIMPAN',
                                style: TextStyle(color: Colors.white,
                                    fontSize: 15.0,
                                    fontFamily: 'Montserrat'),
                              ),
                            ),
                          ),
                        )),
                  ],
                ))
          ],
        )));
  }

}


class getClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();

    path.lineTo(0.0, size.height / 1.9);
    path.lineTo(size.width + 125, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}