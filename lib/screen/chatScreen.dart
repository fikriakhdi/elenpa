import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:elenpa/model/chat.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';


class ChatScreen extends StatefulWidget {
  @override
  ChatScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  ChatScreenState createState() => new ChatScreenState();
}

class ChatScreenState extends State<ChatScreen> with TickerProviderStateMixin {
  List _messages = [];
  final TextEditingController _textController = new TextEditingController();
  bool _isComposing = false;
  String receiverName = "";
  String receiverId = "";
  String myId = "";
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  Future<void> getChatList()async{
  show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String id_user_receiver = prefs.getString('id_user_receiver');
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"receiver_id": id_user_receiver};
    return _netUtil.post(NetworkUtil.CHAT_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        return null;
      } else {
        setState(() {
          receiverName = prefs.getString('user_name_receiver');
          receiverId = prefs.getString('id_user_receiver');
          myId = prefs.getString('userId');
          _messages= res['data'].map((model) => Chat.fromJson(model)).toList();
        });
        dismiss();
      }
    });
  }
  void initState() {
    getChatList();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    ListTile makeListTile(Chat chat) => ListTile(
      contentPadding:
      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),

      leading: (chat.sender_id!=myId?Image.network(NetworkUtil.ASSETS_FILE_URL+chat.sender_photo):null),
      title: Text(
        ""+chat.text,
        style: TextStyle(color: Colors.white,fontSize: 14, fontWeight: FontWeight.bold),
        textAlign: (chat.sender_id==myId?TextAlign.right:TextAlign.left),
      ),
      subtitle: Text(""+chat.date, style: TextStyle(color: Colors.white, fontSize: 12),

        textAlign: (chat.sender_id==myId?TextAlign.right:TextAlign.left)),
      trailing:
      (chat.sender_id==myId?Image.network(NetworkUtil.ASSETS_FILE_URL+chat.sender_photo):null),
    );
    Card makeCard(Chat chat) => Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
        child: makeListTile(chat),
      ),
    );
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(receiverName),
          backgroundColor: DesignCourseAppTheme.grey,
          elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
        ),
        body: (_isShowing? Center(child: CircularProgressIndicator()):new Column(children: <Widget>[
          new Flexible(
              child: new ListView.builder(
                padding: new EdgeInsets.all(8.0),
                reverse: false,
                itemBuilder: (BuildContext context, int index) {
                  return makeCard(_messages[index]);
                },
                itemCount: _messages.length,
              )),
          new Divider(height: 1.0),
          new Container(
            decoration:
            new BoxDecoration(color: Theme.of(context).cardColor),
            child: _buildTextComposer(),
          ),
        ])));
  }

  Widget _buildTextComposer() {
    return new IconTheme(
      data: new IconThemeData(color: Theme.of(context).accentColor),
      child: new Container(
          margin: const EdgeInsets.symmetric(horizontal: 8.0),
          child: new Row(children: <Widget>[           new Flexible(
              child: new TextField(
                controller: _textController,
                onChanged: (String text) {
                  setState(() {
                    _isComposing = text.length > 0;
                  });
                },
                onSubmitted: _isComposing
                    ?_handleSubmitted
                    :null,
                decoration:
                new InputDecoration.collapsed(hintText: "Send a message"),
              ),
            ),
            new Container(
                margin: new EdgeInsets.symmetric(horizontal: 4.0),
                child: Theme.of(context).platform == TargetPlatform.iOS
                    ? new CupertinoButton(
                  child: new Text("Send"),
                  onPressed: _isComposing
                      ? () => _handleSubmitted(_textController.text)
                      : null,
                )
                    : new IconButton(
                  icon: new Icon(Icons.send),
                  onPressed: _isComposing
                      ? () => _handleSubmitted(_textController.text)
                      : null,
                )),
          ]),
          decoration: Theme.of(context).platform == TargetPlatform.iOS
              ? new BoxDecoration(
              border:
              new Border(top: new BorderSide(color: Colors.grey[200])))
              : null),
    );
  }

  Future<void> _handleSubmitted(String text) async{
    _textController.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String id_user_receiver = prefs.getString('id_user_receiver');
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"receiver_id": id_user_receiver, "message":text};
    return _netUtil.post(NetworkUtil.SEND_CHAT_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        return null;
      } else {
        print(res);
        _textController.clear();
        setState(() {
          _isComposing = false;
        });
        setState(() {
//          print(res['data']);
          receiverName = prefs.getString('user_name_receiver');
          _messages = res['data'].map((model) => Chat.fromJson(model)).toList();
//          print(_messages);
        });
      }
    });
  }

}