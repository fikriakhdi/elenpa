
import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:elenpa/utils/network_util.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginScreenState createState() => new _LoginScreenState();
}
final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
class _LoginScreenState extends State<LoginScreen> {
  TextStyle style = TextStyle(fontFamily: 'NanumGothic', fontSize: 20.0);
  bool Authorized = false;
  bool _usernameValid = false;
  bool _passwordValid = false;
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  TextEditingController _controllerUsername = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();
  LocalAuthentication auth = LocalAuthentication();


  NetworkUtil _netUtil = new NetworkUtil();
  final _formKey = GlobalKey<FormState>();

  Future<bool> signin(String username, String password) async{
    show();
    return _netUtil.post(NetworkUtil.LOGIN_URL, body: {
      "username": username,
      "password": password
    }).then((dynamic res) async{
      if(res['status']==400) {
        print("gagal login");
        dismiss();
        return false;
      }
      else {
        dynamic data = res["data"];
        dynamic user = data["user"];
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('token', data["token"]);
        prefs.setString('user_id', user["user_id"]);
        prefs.setString('user_roles', user['user_roles']);
        dismiss();
        if(user['user_roles']=="Perusahaan" || user['user_roles']=="Dinas")
        return true;
        else return false;
      }
    });
  }




  Future<void> _authorize()async{
    bool authenticated = false;
    try{
      authenticated = await auth.authenticateWithBiometrics(
        localizedReason: "Please authenticate to access this app",
        stickyAuth: false,

      );
    } on PlatformException catch(e){
      print(e);
    }
    if(!mounted) return;
    setState(() {
      Authorized = authenticated?true:false;
    });
  }
  @override
  void initState() {
//    _authorize();
    SystemChrome.setEnabledSystemUIOverlays([]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final usernameField = Container(
      width: MediaQuery.of(context).size.width/1.4,
      height: 60,
      // padding: EdgeInsets.only(
      //     top: 4,left: 16, right: 16, bottom: 4
      // ),
      // decoration: BoxDecoration(
      //     borderRadius: BorderRadius.all(
      //         Radius.circular(50)
      //     ),
      //     color: Colors.white,
      //     boxShadow: [
      //       BoxShadow(
      //           color: Colors.black12,
      //           blurRadius: 5
      //       )
      //     ]
      // ),
      child: TextFormField(
        controller: _controllerUsername,
        validator: (value) {
          if (value.isEmpty) {
            return 'Tolong masukkan username Anda';
          }
          return null;
        },
        onChanged: (value) {
          bool usernameValid = value.trim().isNotEmpty;
          if (usernameValid != _usernameValid) {
            setState(() => _usernameValid = usernameValid);
          }
        },
    // style: TextStyle(fontSize: 18.0, color: Color(0xFFbdc6cf)),
        decoration: InputDecoration(
          // border: InputBorder.none,
          border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.black,//this has no effect
                        ),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
          icon: Icon(Icons.person,
            color: Colors.grey,
          ),
          hintText: 'Username',
          filled: true,
          fillColor: Colors.white,
          contentPadding:
              const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(25.7),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(25.7),
          ),
        ),
      ),
    );

    final passwordField = Container(
      width: MediaQuery.of(context).size.width/1.4,
      height: 60,
      margin: EdgeInsets.only(top: 10),
      // padding: EdgeInsets.only(
      //     top: 4,left: 16, right: 16, bottom: 4
      // ),
      // decoration: BoxDecoration(
      //     borderRadius: BorderRadius.all(
      //         Radius.circular(50)
      //     ),
      //     color: Colors.white,
      //     boxShadow: [
      //       BoxShadow(
      //           color: Colors.black12,
      //           blurRadius: 5
      //       )
      //     ]
      // ),
      child: TextFormField(
        obscureText: true,
        controller: _controllerPassword,
        validator: (value) {
          if (value.isEmpty) {
            return 'Tolong masukkan kata sandi Anda';
          }
          return null;
        },
        onChanged: (value) {
          bool passwordValid = value.trim().isNotEmpty;
          if (passwordValid != _passwordValid) {
            setState(() => _passwordValid = passwordValid);
          }
        },
    // style: TextStyle(fontSize: 18.0, color: Color(0xFFbdc6cf)),
        decoration: InputDecoration(
          border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.black,//this has no effect
                        ),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
          icon: Icon(Icons.vpn_key,
            color: Colors.grey,
          ),
          hintText: 'Password',
          filled: true,
          fillColor: Colors.white,
          contentPadding:
              const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(25.7),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(25.7),
          ),
        ),
      ),
    );

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color.fromRGBO(76, 162, 205, 1),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width/2,
        padding: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 15.0),
        onPressed: ()async {

          if(!_usernameValid || !_passwordValid){
            _formKey.currentState.validate();
            _scaffoldState.currentState.showSnackBar(
              SnackBar(
                content: Text("Please Fill all field"),
              ),
            );
            return;
          } else {

            String username = _controllerUsername.text.toString().toLowerCase();
            String password = _controllerPassword.text.toString();
              signin(username, password).then((isSuccess) {
                if (isSuccess) {
//                  Navigator.push(
//                    context,
//                    MaterialPageRoute(builder: (context) => HomeScreen()),
//                  );
                  Navigator.of(context).pushReplacementNamed('/CreatePassCodeScreen');
                } else {
                  _scaffoldState.currentState.showSnackBar(
                    SnackBar(
                      content: Text("Invalid Username/Password"),
                    ),
                  );
                }
              });
          }
        },
        child: Text("LOG IN",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );
    return
        Scaffold(
      key: _scaffoldState,
      body: Center(
       child: Form(
       key:_formKey,
       child:(_isShowing? Center(child: CircularProgressIndicator()):ListView(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height/2.5,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color.fromRGBO(76, 162, 205, 1),
                      Color.fromRGBO(243, 254, 174, 1)
                    ],
                  ),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(90)
                  )
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Spacer(),
                  Align(
                    alignment: Alignment.center,
                  child: Image.asset(
                    "assets/images/logo.png",
                    fit: BoxFit.contain,
                  ),
                  ),
                  Spacer(),

//                  Align(
//                    alignment: Alignment.bottomRight,
//                    child: Padding(
//                      padding: const EdgeInsets.only(
//                          bottom: 32,
//                          right: 32
//                      ),
//                      child: Text('Login',
//                        style: TextStyle(
//                            color: Colors.white,
//                            fontSize: 18
//                        ),
//                      ),
//                    ),
//                  ),
                ],
              ),
            ),

            Container(
              height: MediaQuery.of(context).size.height/2,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only( top: 62),
              child: Column(
                children: <Widget>[
                  usernameField,
                  passwordField,
                  // Align(
                  //   alignment: Alignment.centerRight,
                  //   child: Padding(
                  //     padding: const EdgeInsets.only(
                  //         top: 16, right: 32
                  //     ),
                  //     child: Text('Forgot Password ?',
                  //       style: TextStyle(
                  //           color: Colors.grey
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  Spacer(),

                  loginButon,
                ],
              ),
            )
          ],
        )),
       ),
      )
    );
  }
//  @override
//  Widget build(BuildContext context) {
//    final emailField = TextFormField(
//      controller: _controllerUsername,
//      validator: (value) {
//        if (value.isEmpty) {
//          return 'Please input your email';
//        }
//        return null;
//      },
//      obscureText: false,
//      style: style,
//      onChanged: (value) {
//        bool usernameValid = value.trim().isNotEmpty;
//        if (usernameValid != _usernameValid) {
//          setState(() => _usernameValid = usernameValid);
//        }
//      },
//      decoration: InputDecoration(
//          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
//          hintText: "Username",
//          border:
//          OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
//
//    );
//    final passwordField = TextFormField(
//      controller: _controllerPassword,
//      validator: (value) {
//        if (value.isEmpty) {
//          return 'Please input your password';
//        }
//        return null;
//      },
//      obscureText: true,
//      style: style,
//      onChanged: (value) {
//        bool passwordValid = value.trim().isNotEmpty;
//        if (passwordValid != _passwordValid) {
//          setState(() => _passwordValid = passwordValid);
//        }
//      },
//      decoration: InputDecoration(
//          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
//          hintText: "Password",
//          border:
//          OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
//    );

//
//    return Scaffold(
//      key: _scaffoldState,
//      body: Center(
//        child: Form(
//        key:_formKey,
//          child: Padding(
//            padding: const EdgeInsets.all(30.0),
//            child: Column(
//              crossAxisAlignment: CrossAxisAlignment.center,
//              mainAxisAlignment: MainAxisAlignment.center,
//              children: <Widget>[
//                SizedBox(
//                  height: 155.0,
//                  child: Image.asset(
//                    "assets/images/logo.png",
//                    fit: BoxFit.contain,
//                  ),
//                ),
//                SizedBox(height: 25.0),
//                emailField,
//                SizedBox(height: 15.0),
//                passwordField,
//                SizedBox(
//                  height: 35.0,
//                ),
//                loginButon,
//                SizedBox(
//                  height: 15.0,
//                ),
//              ],
//            ),
//          ),
//        ),
//      ),
//    );
//  }
}