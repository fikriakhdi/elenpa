import 'package:flutter/material.dart';
import 'package:async/async.dart';
import 'package:elenpa/model/user_pin.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path/path.dart' as Path;
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  startTime() async {
    var _duration = new Duration(seconds: 2);

    return new RestartableTimer(_duration, navigationPage);
  }






  void navigationPage() {

      Navigator.of(context).pushReplacementNamed('/DirectScreen');
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Image.asset('assets/images/logo.png'),
      ),
    );
  }
}