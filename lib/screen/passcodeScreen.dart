import 'package:flutter/material.dart';
import 'package:flutter_verification_code_input/flutter_verification_code_input.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';
import 'package:elenpa/theme/theme.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
class passcodeScreen extends StatefulWidget {
  @override
  passcodeScreenState createState() => new passcodeScreenState();
}

class passcodeScreenState extends State<passcodeScreen> {
  String _onCompleted = "";
  bool Authorized = false;
  bool sippaFlag = false;
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  LocalAuthentication auth = LocalAuthentication();
  bool _availableBuimetricType = false;
  NetworkUtil _netUtil = new NetworkUtil();
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

  Future<void> _getAvailableSupport() async {
    // 7. this method fetches all the available biometric supports of the device
    List<BiometricType> availableBuimetricType = List<BiometricType>();
    try {
      availableBuimetricType =
      await auth.getAvailableBiometrics();
    } catch (e) {
      print(e);
    }
    if (!mounted) return false;
    setState(() {
      _availableBuimetricType = (availableBuimetricType.length>0?true:false);
    });
  }

  Future<void> _authorize()async{
    bool authenticated = false;
    try{
      authenticated = await auth.authenticateWithBiometrics(
        localizedReason: "Tolong Scan Sidik jari anda untuk masuk",
        stickyAuth: false,

      );
    } on PlatformException catch(e){
      print(e);
    }
    if(!mounted) return;
    if(authenticated) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String P3NPA_PIN = prefs.getString("P3NPA_PIN");
      checkPin(P3NPA_PIN).then((ret) {
        if (ret) {
          Navigator.of(context).pushReplacementNamed('/HomeScreen');
        } else {
          dismiss();
          _scaffoldState.currentState.showSnackBar(
            SnackBar(
              content: Text("PIN Salah!"),
            ),
          );
        }
      });
    }
    setState(() {
      Authorized = authenticated?true:false;
    });
  }

  Future<bool> checkPin(String PIN) async{
    show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String user_id = prefs.getString("user_id");
    String user_roles = prefs.getString("user_roles");
    String sippa_file = prefs.getString("sippa_file");
    String P3NPA_PIN = prefs.getString("P3NPA_PIN");
    if(PIN==P3NPA_PIN) {
      print("user_id");
      print(user_id);
      return _netUtil.post(NetworkUtil.CHECK_PIN_URL, body: {
        "user_id": user_id,
        "pin": PIN
      }).then((dynamic res) async {
        if (res['status'] == 400) {
          dismiss();
          return false;
        }
        else {
          print("Role :");
          print(user_roles);
          print(res);
          setState(() {
            if(user_roles=="Perusahaan"){
              if(sippa_file=="true") sippaFlag=true;
              else sippaFlag=false;
            } else  sippaFlag=true;
          });
          dismiss();
          return true;
        }
      });
    } else  {dismiss();
      return false;
    }
  }

  @override
  void initState() {
    _getAvailableSupport();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key:_scaffoldState,
      appBar: new AppBar(
        title: Center(child: new Text('Masukkan PIN Anda')),
        backgroundColor: DesignCourseAppTheme.grey,
      ),
      body: (_isShowing? Center(child: CircularProgressIndicator()):Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Text(
                'Masukkan PIN Anda',
                style: TextStyle(fontSize: 20.0),
              ),
            ),
          ),
          VerificationCodeInput(
            keyboardType: TextInputType.number,
            length: 4,
            autofocus: false,
            onCompleted: (String value) {
              checkPin(value).then((ret){
                if(ret){
                  print("pass");
                  if(sippaFlag)
                    Navigator.of(context).pushReplacementNamed('/HomeScreen');
                    else
                    Navigator.of(context).pushReplacementNamed('/FirstUploadSippaScreen');
                } else {
                  _scaffoldState.currentState.showSnackBar(
                    SnackBar(
                      content: Text("PIN Salah!"),
                    ),
                  );
                }
              });
              //direct to main page
            },
          ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _availableBuimetricType?InkWell(
              onTap: () {_authorize();},
              child: Container(
                margin: const EdgeInsets.all(10.0),
                child: Center(
                  child: Image.asset('assets/images/fingerprint.png',
                      width: 50.0, height: 50.0),
                ),
              ),
            ):SizedBox(height: 25.0),
          ),
        ],
      )),
    );
  }
}