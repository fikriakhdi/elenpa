
import 'package:flutter/material.dart';
import 'package:elenpa/theme/theme.dart';
import 'dart:async';
import 'package:elenpa/utils/network_util.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
class InputVolumeScreen extends StatefulWidget {

  final String title ="Input Volume";

  @override
  _InputVolumeScreenState createState() => new _InputVolumeScreenState();
}
final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
class _InputVolumeScreenState extends State<InputVolumeScreen> {
  NetworkUtil _netUtil = new NetworkUtil();
final TextRecognizer textRecognizer = FirebaseVision.instance.textRecognizer();
  final meteran_byte = null; //Base64Decoder().convert(base64Image);
  Widget image_meteran = null; //Image.memory(_byteImage)
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  File _image;
  var formatter = new DateFormat('yyyy-MM-dd');
  TextEditingController volume_meter = new TextEditingController();
  TextEditingController tanggal_pemakaian = new TextEditingController();
  int _selectedIntake = null;
  String photo_meteran = "";
  String berita_acara = "";
  dynamic User = {"nama_lengkap":"", "email":""};
  List<DropdownMenuItem<int>> intakeList = [];
  List<DropdownMenuItem<int>> cityList = [];
  Future cameraImage() async {
    show();
    var image = await ImagePicker.pickImage(
      source: ImageSource.camera
    ).then((image_res)async{
      dismiss();
    setState(() {
      _image = image_res;

    });
  });
  }

  Future getOCR() async{
    show();
    FirebaseVisionImage ourImage = FirebaseVisionImage.fromFile(_image);
    TextRecognizer recognizeText = FirebaseVision.instance.textRecognizer();
    VisionText readText = await recognizeText.processImage(ourImage).then((onValue){
      dismiss();
    setState(() {
    volume_meter.text= onValue.text.toString();
    });
    });
}

    Future<void> selectDate()async{        
      var date = DateTime.now(); 
        final DateTime d = await showDatePicker(
            context: context,
            
          firstDate:new DateTime(date.year, date.month - 12, date.day),
          initialDate: DateTime.now(),
          lastDate: DateTime.now()
            );
            
    if (d != null) //if the user has selected a date
      setState(() {
        // we format the selected date and assign it to the state variable
        tanggal_pemakaian.text = formatter.format(d);
      });
    }

  Future<void> loadIntakeList() async{
    show();
    intakeList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    return _netUtil.get(NetworkUtil.INTAKE_URL,headers).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        return null;
      } else {
        setState(() {
          print(res['data']);
          res['data'].map((model) => intakeList.add(new DropdownMenuItem(
            child: new Text(model['sumber_mata_air']),
            value: int.parse((model['intake_id']!=null?model['intake_id']:0)),
          ))).toList();
        });
        dismiss();
      }
    });
  }


  Future<void> PostInputVolume(String intake, volume_meter, tanggal_pemakaian, foto) async{
    show();
    cityList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"intake": intake,"volume_meter":volume_meter, "tanggal_pemakaian":tanggal_pemakaian, "foto":foto};
    return _netUtil.post(NetworkUtil.INPUT_VOLUME_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        return null;
      } else {
        dismiss();
        return true;
      }
    });
  }

  @override
  void initState() {
    show();
    var now = new DateTime.now();
    tanggal_pemakaian.text = formatter.format(now);
    loadIntakeList();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {



    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );
    return new Scaffold(
        key: _scaffoldState,
        appBar: topAppBar,
        body: (_isShowing? Center(child: CircularProgressIndicator()):new ListView(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: DropdownButton(
                        hint: new Text('Pilih Intake'),
                        items: intakeList,
                        value: _selectedIntake,
                        onChanged: (value) {
                          setState(() {
                            _selectedIntake = value;
                          });
                        },
                        isExpanded: true,
                      ),

                    ),
                    TextFormField(
                      controller:volume_meter,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Tolong Masukkan Volume Meter';
                        }
                        return null;
                      },

                      decoration: new InputDecoration(hintText: "Volume Meter", contentPadding: const EdgeInsets.all(20.0)),
                    ),
                    TextFormField(
                      onTap: (){
                        selectDate();
                      },
                      controller:tanggal_pemakaian,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Tolong Masukkan Tanggal Pemakaian';
                        }
                        return null;
                      },
                    
                      decoration: new InputDecoration(hintText: "Tanggal Pemakaian", contentPadding: const EdgeInsets.all(20.0)),
                    ),
                    SizedBox(height: 15.0),
                    Container(
                        height: 200.0,
                        width: 300.0,
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.grey,
                          elevation: 7.0,
                          child: GestureDetector(
                            onTap: () {
                              cameraImage().then((ret){getOCR();});
                            },
                            child: _image!=null?new Image.file(_image):Image.asset('assets/images/noimage.jpg'),
                          ),
                        )),
                    SizedBox(height: 15.0),
                    Container(
                        height: 45.0,
                        width: 115.0,
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          shadowColor: Colors.greenAccent,
                          color: Colors.green,
                          elevation: 7.0,
                          child: GestureDetector(
                            onTap: () {
                              PostInputVolume(_selectedIntake.toString(),volume_meter.text,tanggal_pemakaian.text, 'data:image/png;base64,' +
                                  'data:image/png;base64,' +base64Encode(_image.readAsBytesSync())).then((res){
                                _scaffoldState.currentState.showSnackBar(
                                    SnackBar(
                                      content: Text("Update Success"),
                                    ),
                                );
                              });
                            },
                            child: Center(
                              child: Text(
                                'Kirim',
                                style: TextStyle(color: Colors.white,
                                    fontSize: 15.0,
                                    fontFamily: 'Montserrat'),
                              ),
                            ),
                          ),
                        )),
                  ],
                ))
          ],
        )));
  }

}

