import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:elenpa/model/sippa.dart';
import 'package:loadmore/loadmore.dart';
import 'dart:async';

class SippaScreen extends StatefulWidget {
  SippaScreen() : super();

  final String title = "SIPPA";

  @override
  SippaScreenState createState() => SippaScreenState();
}

class SippaScreenState extends State<SippaScreen> {
  bool _isShowing = false;
  bool perbarui = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  List sippa=[];
  List selectedSippa;
  bool sort;
  int total = 100;
  int start = 0;
  int length = 10;
  int get count => sippa.length;
  Future<void> getSippa() async{
    NetworkUtil _netUtil = new NetworkUtil();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    String user_roles = prefs.getString('user_roles');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"start": start.toString(), "length":length.toString()};
    _netUtil.post(NetworkUtil.SIPPA_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print(res);
        print("Error to get data");
        dismiss();
        return null;
      } else {
        setState(() {
          if(res['data'].length>0) {
            start += 10;
            total = res['data'][0]['total_row'];
            sippa += res['data'].map((model) => Sippa.fromJson(model)).toList();
            if(user_roles=="Perusahaan") perbarui=true;
            else perbarui=false;
//          print(res['data'][0]);
          }
        });
        dismiss();
      }
    });
  }
  @override
  void initState() {
    show();
    sort = false;
    selectedSippa = [];
    getSippa();
    super.initState();
  }

  onSortColum(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        sippa.sort((a, b) => a.nama_lengkap.compareTo(b.nama_lengkap));
      } else {
        sippa.sort((a, b) => b.nama_lengkap.compareTo(a.nama_lengkap));
      }
    }
  }




  @override
  Widget build(BuildContext context) {

    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );
    return Scaffold(
      appBar: topAppBar,
      body: (_isShowing? Center(child: CircularProgressIndicator()):sippa.length>0?Container(
        child: RefreshIndicator(
          child: LoadMore(
            isFinish: count >= total,
            onLoadMore: _loadMore,
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return Card(
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
//                              leading: const Icon(Icons.data_usage),
                            title: Text('${sippa[index].nomor_sippa}',style: TextStyle(fontWeight: FontWeight.bold)),
                            subtitle: Text('Sippa Awal : ${sippa[index].nomor_sippa_awal}\nTanggal Penetapan : ${sippa[index].tanggal_penetapan}\nSumber Mata Air : ${sippa[index].sumber_mata_air}'),
                            contentPadding: EdgeInsets.all(20.0),
                            dense: false,
                            isThreeLine: true,
                            trailing: Icon(FontAwesomeIcons.file),
                            onTap:(){
                              viewSippa(sippa[index].id);
                            },
                          ),Center(child: Text(sippa[index].status=="aktif"?"AKTIF":"SUDAH HABIS", style: TextStyle(color: sippa[index].status=="aktif"?Colors.green:Colors.red, fontWeight: FontWeight.bold))),
                          SizedBox(
                            height: 16,
                          ),
                        ]));
              },
              itemCount: count,
            ),
            whenEmptyLoad: false,
            delegate: DefaultLoadMoreDelegate(),
            textBuilder: DefaultLoadMoreTextBuilder.english,
          ),
          onRefresh: _refresh,
        ),
      ):Center(child: Text("Tidak ada data"))),

      floatingActionButton: perbarui?FloatingActionButton.extended(
        onPressed: () {
          // Add your onPressed code here!
                  Navigator.of(context).pushNamed('/RenewSippaScreen');
        },
        label: Text('PERBARUI'),

        icon: Icon(Icons.add),
        backgroundColor: Colors.green,
      ):null,
    );
  }
  Future<bool> _loadMore() async {
    print("onLoadMore");
    await Future.delayed(Duration(seconds: 0, milliseconds: 100));
    getSippa();
    return true;
  }

Future<void> viewSippa(String id) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('edit_sippa', id);
    Navigator.pushNamed(
        context, '/detailSippaScreen'
    );
  }
  Future<void> _refresh() async {
    print("refresh");
    await Future.delayed(Duration(seconds: 0, milliseconds: 2000));
    sippa.clear();
    setState(() {
      start = 0;
      length = 10;
    });
    getSippa();
  }
}

