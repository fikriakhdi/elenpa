import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:elenpa/model/file_sippa.dart';
import 'package:loadmore/loadmore.dart';
import 'dart:async';

class UploadSippaScreen extends StatefulWidget {
  UploadSippaScreen() : super();

  final String title = "Unggah SIPPA";

  @override
  UploadSippaState createState() => UploadSippaState();
}

class UploadSippaState extends State<UploadSippaScreen> {
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  List sippa=[];
  List selectedSippa;
  bool sort;
  int total = 100;
  int start = 0;
  int length = 10;
  int get count => sippa.length;
  Future<void> getSippa() async{
    NetworkUtil _netUtil = new NetworkUtil();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"start": start.toString(), "length":length.toString()};
    return _netUtil.post(NetworkUtil.UPLOADED_SIPPA_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print(res);
        print("Error to get data");
        dismiss();
        return null;
      } else {
        
        print(res);
        setState(() {
          if(res['data'].length>0) {
            start += 10;
            total = res['data'][0]['total_row'];
            sippa += res['data'].map((model) => File_Sippa.fromJson(model)).toList();
          }
        });
        dismiss();
      }
    });
  }
  @override
  void initState() {
    show();
    sort = false;
    selectedSippa = [];
    getSippa();
    super.initState();
  }





  @override
  Widget build(BuildContext context) {

    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );
    return Scaffold(
      appBar: topAppBar,
      body: (_isShowing? Center(child: CircularProgressIndicator()):sippa.length>0?Container(
        child: RefreshIndicator(
          child: LoadMore(
            isFinish: count >= total,
            onLoadMore: _loadMore,
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return Card(
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
//                              leading: const Icon(Icons.data_usage),
                            title: Text('${sippa[index].nama_perusahaan}',style: TextStyle(fontWeight: FontWeight.bold)),
                            subtitle: Text('Diunggah pada : ${sippa[index].tanggal}'),
                            trailing:
                            Icon(Icons.file_download, color: Colors.black, size: 30.0),
                            onTap: () async{
                              await launch(NetworkUtil.ASSETS_FILE_URL+sippa[index].file_url);
                            },
                            contentPadding: EdgeInsets.all(20.0),
                            dense: false,
                            isThreeLine: true,
                          ),
                          SizedBox(
                            height: 2,
                          ),
                        ]));
              },
              itemCount: count,
            ),
            whenEmptyLoad: false,
            delegate: DefaultLoadMoreDelegate(),
            textBuilder: DefaultLoadMoreTextBuilder.english,
          ),
          onRefresh: _refresh,
        ),
      ):Center(child: Text("Tidak ada data"))),

      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          // Add your onPressed code here!
                  Navigator.of(context).pushNamed('/NewUploadSippaScreen');
        },
        label: Text('Unggah'),

        icon: Icon(Icons.add),
        backgroundColor: Colors.green,
      ),
    );
  }
  Future<bool> _loadMore() async {
    print("onLoadMore");
    await Future.delayed(Duration(seconds: 0, milliseconds: 100));
    getSippa();
    return true;
  }

  Future<void> _refresh() async {
    print("refresh");
    await Future.delayed(Duration(seconds: 0, milliseconds: 2000));
    sippa.clear();
    setState(() {
      start = 0;
      length = 10;
    });
    getSippa();
  }
}

