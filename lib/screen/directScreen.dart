import 'package:flutter/material.dart';
import 'package:flutter_verification_code_input/flutter_verification_code_input.dart';
import 'package:shared_preferences/shared_preferences.dart';
class DirectScreen extends StatefulWidget {
  @override
  DirectScreenState createState() => new DirectScreenState();
}

class DirectScreenState extends State<DirectScreen> {
  String _onCompleted = "";

  Future<void> pinCheck() async{

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var check =  prefs.getString('P3NPA_PIN');
    if(check==null || check==""){
      //navigate to LoginScreen to login and create passcode
      Navigator.of(context).pushReplacementNamed('/LoginScreen');
    } else {
      Navigator.of(context).pushReplacementNamed('/PasscodeScreen');
    }
  }

  @override
  void initState() {
    super.initState();
    pinCheck();
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
    );
  }
}