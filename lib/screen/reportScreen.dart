import 'package:flutter/material.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';

class ReportScreen extends StatefulWidget {


  String title = "Laporan";
  @override
  _ReportScreenState createState() => new _ReportScreenState();
}

class _ReportScreenState extends State<ReportScreen> {
  bool _isShowing = false;
  NetworkUtil _netUtil = new NetworkUtil();
  dynamic User = {"nama_lengkap":"", "email":""};
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }

  Future<void> getProfileData() async{
    show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    return _netUtil.get(NetworkUtil.PROFILE_URL, headers).then((
        dynamic res) async {
      if (res['status'] == 400) {
        print("Error to get profile data");
      } else{
        setState(() {
          User = res['data'];
        });
        dismiss();
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    final makeBody = Container(
      // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
      child: ListView(
        children:<Widget>[
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
              child: ListTile(
                contentPadding:
                EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                title: Text(
                  "Pajak Bulan Lalu",
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                ),
                trailing:
                Icon(Icons.update, color: Colors.white, size: 30.0),
                onTap: () {
                  //redirect
                  Navigator.pushNamed(context, '/PajakBulanLaluScreen');
                },
              ),
            ),
          ),
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
              child: ListTile(
                contentPadding:
                EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                title: Text(
                  "Pajak Bulan Ini",
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                ),
                trailing:
                Icon(Icons.date_range, color: Colors.white, size: 30.0),
                onTap: () {
                  //redirect
                  Navigator.pushNamed(context, '/PajakBulanIniScreen');
                },
              ),
            ),
          )
        ]
      ),
    );

    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );

    return Scaffold(
      backgroundColor: DesignCourseAppTheme.grey,
      appBar: topAppBar,
      body: (_isShowing? Center(child: CircularProgressIndicator()):makeBody),
    );
  }
}



