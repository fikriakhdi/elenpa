import 'package:flutter/material.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';

class MapMenuScreen extends StatefulWidget {


  String title = "Menu Map";
  @override
  _MapMenuScreenState createState() => new _MapMenuScreenState();
}

class _MapMenuScreenState extends State<MapMenuScreen> {
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    final makeBody = Container(
      // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
      child: ListView(
        children:<Widget>[
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
              child: ListTile(
                contentPadding:
                EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                title: Text(
                  "Lihat Map",
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                ),
                trailing:
                Icon(Icons.map, color: Colors.white, size: 30.0),
                onTap: () {
                  //redirect
                  Navigator.pushNamed(context, '/MapScreen');
                },
              ),
            ),
          ),
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
              child: ListTile(
                contentPadding:
                EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                title: Text(
                  "Ubah Lokasi Intake",
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                ),
                trailing:
                Icon(Icons.update, color: Colors.white, size: 30.0),
                onTap: () {
                  //redirect
                  Navigator.pushNamed(context, '/UbahLokasiIntakeScreen');
                },
              ),
            ),
          )
        ]
      ),
    );

    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );

    return Scaffold(
      backgroundColor: DesignCourseAppTheme.grey,
      appBar: topAppBar,
      body: (_isShowing? Center(child: CircularProgressIndicator()):makeBody),
    );
  }
}



