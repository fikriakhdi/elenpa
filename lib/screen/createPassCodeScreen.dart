import 'package:flutter/material.dart';
import 'package:flutter_verification_code_input/flutter_verification_code_input.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';
import 'package:elenpa/theme/theme.dart';
import 'dart:core';


class CreatePassCodeScreen extends StatefulWidget {
  @override
  CreatePassCodeScreenState createState() => new CreatePassCodeScreenState();
}

class CreatePassCodeScreenState extends State<CreatePassCodeScreen> {
  String _onCompleted = "";
  bool sippaFile = false;
  NetworkUtil _netUtil = new NetworkUtil();
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  Future<bool> setPin(String PIN) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var user_id = prefs.getString('user_id');
    var user_roles = prefs.getString('user_roles');
    return _netUtil.post(NetworkUtil.CREATE_PIN_URL, body: {
      "user_id": user_id,
      "pin": PIN
    }).then((dynamic res) async{
      if(res['status']==400) {
        print("something went wrong");
        return false;
      }
      else {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        setState(() {
          if(user_roles=="Perusahaan") sippaFile=false;
          else sippaFile=true;
          prefs.setString('P3NPA_PIN', PIN);
        });
        return true;
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Center(child: new Text('Buat PIN Anda')),
        backgroundColor: DesignCourseAppTheme.grey,
      ),
          key: _scaffoldState,
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Text(
                'Masukkan PIN',
                style: TextStyle(fontSize: 20.0),
              ),
            ),
          ),
          VerificationCodeInput(
            keyboardType: TextInputType.number,
            length: 4,
            autofocus: false,
            onCompleted: (String value) {
              setPin(value).then((res){
                if(res==true){
                  //redirect to main menu
                  if(sippaFile)
                  Navigator.of(context).pushReplacementNamed('/HomeScreen');
                  else
                  Navigator.of(context).pushReplacementNamed('/FirstUploadSippaScreen');
                } else {
                  _scaffoldState.currentState.showSnackBar(
                      SnackBar(
                        content: Text("Failed to set PIN"),
                      ),
                  );
                }
              });
            },
          ),
        ],
      ),
    );
  }
}