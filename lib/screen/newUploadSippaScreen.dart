import 'package:elenpa/screen/homeScreen.dart';
import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:elenpa/model/user.dart';
import 'dart:async';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:http/http.dart' show Client;
import 'package:elenpa/utils/network_util.dart';
import 'package:elenpa/model/user.dart';
import 'package:elenpa/model/session.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:file_picker/file_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:convert';

class NewUploadSippaScreen extends StatefulWidget {
//  ProfileScreen({Key key, this.title}) : super(key: key);

  final String title ="Unggah Sippa Baru";

  @override
  _NewUploadSippaScreenState createState() => new _NewUploadSippaScreenState();
}
final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
class _NewUploadSippaScreenState extends State<NewUploadSippaScreen> {
  NetworkUtil _netUtil = new NetworkUtil();
  File _image;
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  TextEditingController nama_lengkap = new TextEditingController();
  TextEditingController alamat = new TextEditingController();
  TextEditingController notelp = new TextEditingController();
  TextEditingController email = new TextEditingController();
  String _fileName;
  String _path;
  Map<String, String> _paths;
  String _extension;
  bool _loadingPath = false;
  bool _multiPick = false;
  bool _hasValidMime = false;
  FileType _pickingType;
  TextEditingController _controller = new TextEditingController();
  int _selectedPerusahaan = null;
  int _selectedCity = null;
  dynamic User = {"nama_lengkap":"", "email":""};
  List<DropdownMenuItem<int>> perusahaanList = [];
  List<DropdownMenuItem<int>> cityList = [];
  String FileUrl = "";
  String filePath;

Future cameraImage() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.camera
    );

    setState(() {
      _image = image;
    });
  }
  Future<void> loadPerusahaanList() async{
    show();
    perusahaanList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    return _netUtil.get(NetworkUtil.PERUSAHAAN_URL,headers).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get data");
        dismiss();
        return null;
      } else {
        setState(() {
          print(res['data']);
          res['data'].map((model) => perusahaanList.add(new DropdownMenuItem(
            child: new Text(model['nama_lengkap']),
            value: int.parse((model['user_id']!=null?model['user_id']:0)),
          ))).toList();
        });
        dismiss();
      }
    });
  }

 
  Future<bool> postUploadSippa(String intake, foto) async{
    show();
    cityList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String token = prefs.getString('token');
    Map<String, String> headers = {"token": token};
    Map<String, String> body = {"perusahaan_id":intake,"foto":foto};
    return _netUtil.post(NetworkUtil.UPLOAD_SIPPA_URL,headers:headers, body:body).then((dynamic res) async {
      print(res);
      if(res['status']==400) {
        dismiss();
        return null;
      } else {
        dismiss();
        return true;
      }
    });
  }

  @override
  void initState() {
    show();
    loadPerusahaanList();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );
    return new Scaffold(
        key: _scaffoldState,
        appBar: topAppBar,
        body: (_isShowing? Center(child: CircularProgressIndicator()):new ListView(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 15.0),
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: DropdownButton(
                      hint: new Text('Pilih Perusahaan'),
                      items: perusahaanList,
                      value: _selectedPerusahaan,
                      onChanged: (value) {
                        setState(() {
                          _selectedPerusahaan = value;
                        });
                      },
                      isExpanded: true,
                    ),
                    ),
                    Container(
                        height: 190.0,
                        width: 300.0,
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.grey,
                          elevation: 7.0,
                          child: GestureDetector(
                            onTap: () {
                              cameraImage();
                            },
                            child: _image!=null?new Image.file(_image):Image.asset('assets/images/noimage.jpg'),
                          ),
                        )),
                    SizedBox(height: 60.0),
                    Container(
                        height: 45.0,
                        width: 115.0,
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          shadowColor: Colors.greenAccent,
                          color: Colors.green,
                          elevation: 7.0,
                          child: GestureDetector(
                            onTap: () {
                              postUploadSippa(_selectedPerusahaan.toString(), 'data:image/png;base64,' +
                                  base64Encode(_image.readAsBytesSync())).then((res){
                                    if(res){
                                _scaffoldState.currentState.showSnackBar(
                                    SnackBar(
                                      content: Text("Unggah Sukses"),
                                    ),
                                );
                                    } else {
                                _scaffoldState.currentState.showSnackBar(
                                    SnackBar(
                                      content: Text("Unggah Gagal"),
                                    ),
                                );
                                    }
                              });
                            },
                            child: Center(
                              child: Text(
                                'Kirim SIPPA',
                                style: TextStyle(color: Colors.white,
                                    fontSize: 15.0,
                                    fontFamily: 'Montserrat'),
                              ),
                            ),
                          ),
                        )),
                  ],
                ))
          ],
        )));
  }

}
