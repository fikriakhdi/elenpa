import 'package:elenpa/theme/theme.dart';
import 'package:elenpa/model/category.dart';
import 'package:flutter/material.dart';

class PerusahaanListView extends StatefulWidget {
  final Function callBack;

  const PerusahaanListView({Key key, this.callBack}) : super(key: key);
  @override
  PerusahaanListViewState createState() => PerusahaanListViewState();
}

class PerusahaanListViewState extends State<PerusahaanListView>
    with TickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController = AnimationController(
        duration: Duration(milliseconds: 5000), vsync: this);
    super.initState();
  }

  Future<bool> getData() async {
    await Future.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: FutureBuilder(
        future: getData(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return SizedBox();
          } else {
            return GridView(
              padding: EdgeInsets.all(10),
              scrollDirection: Axis.vertical,
              children: List.generate(
                Category.MenuPerusahaan.length,
                    (index) {
                  var count = Category.MenuPerusahaan.length;
                  var animation = Tween(begin: 0.0, end: 1.0).animate(
                    CurvedAnimation(
                      parent: animationController,
                      curve: Interval((1 / count) * index, 1.0,
                          curve: Curves.fastOutSlowIn),
                    ),
                  );
                  animationController.forward();
                  return CategoryView(
                    callback: () {
                      widget.callBack();
                    },
                    category: Category.MenuPerusahaan[index],
                    animation: animation,
                    animationController: animationController,
                  );
                },
              ),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 32.0,
                crossAxisSpacing: 32.0,
                childAspectRatio: 0.8,
              ),
            );
          }
        },
      ),
    );
  }
}

class CategoryView extends StatelessWidget {
  final VoidCallback callback;
  final Category category;
  final AnimationController animationController;
  final Animation animation;

  const CategoryView(
      {Key key,
        this.category,
        this.animationController,
        this.animation,
        this.callback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: animationController,
        builder: (BuildContext context, Widget child) {
          return FadeTransition(
            opacity: animation,
            child: new Transform(
                transform: new Matrix4.translationValues(
                    0.0, 50 * (1.0 - animation.value), 0.0),
                child: InkWell(
                  splashColor: Colors.transparent,
                  onTap: () {
//                callback();
                    Navigator.pushNamed(
                      context, '/' + category.screen,
                    );
                  },
                  child: SizedBox(
                    height: 280,
                    child: Stack(
                      alignment: AlignmentDirectional.bottomCenter,
                      children: <Widget>[
                        Container(
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  decoration: new BoxDecoration(
                                    color: DesignCourseAppTheme.grey,
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(16.0)),
                                    // border: new Border.all(
                                    //     color: DesignCourseAppTheme.notWhite),
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                          child: Column(
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 16,
                                                    left: 16,
                                                    right: 16),
                                                child: Text(
                                                  category.title,
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16,
                                                    letterSpacing: 0.27,
                                                    color: DesignCourseAppTheme
                                                        .nearlyWhite,
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 8,
                                                    left: 16,
                                                    right: 16,
                                                    bottom: 8),
                                                child: Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 48,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 48,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding:
                            const EdgeInsets.only(
                                right: 16, left: 16, bottom: 10),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(16.0)),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                      color: DesignCourseAppTheme.grey
                                          .withOpacity(0.3),
                                      offset: Offset(0.0, 0.0),
                                      blurRadius: 6.0),
                                ],
                              ),
                              child: ClipRRect(
                                borderRadius:
                                BorderRadius.all(Radius.circular(16.0)),
                                child: AspectRatio(
                                    aspectRatio: 1.28,
                                    child: Image.asset(category.imagePath,
                                        gaplessPlayback: true)),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
            ),
          );
        });
  }
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
