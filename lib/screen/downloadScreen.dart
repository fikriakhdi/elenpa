import 'package:flutter/material.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:elenpa/model/download.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';

class DownloadScreen extends StatefulWidget {


  String title = "Download";
  @override
  _DownloadScreenState createState() => new _DownloadScreenState();
}

class _DownloadScreenState extends State<DownloadScreen> {
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  List downloads=[];
  Future<void> getDownloads() async{
    show();
    NetworkUtil _netUtil = new NetworkUtil();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    return _netUtil.get(NetworkUtil.DOWNLOAD_URL,headers).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        return null;
      } else {
        setState(() {
          downloads= res['data'].map((model) => Download.fromJson(model)).toList();
        });
        dismiss();
      }
    });
  }

  @override
  void initState() {
    getDownloads();
//  downloads = getLessons();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ListTile makeListTile(Download download) => ListTile(
      contentPadding:
      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      title: Text(
        ""+download.keterangan.substring(0, 25)+"..",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      subtitle: Text(""+download.tanggal_upload, style: TextStyle(color: Colors.white)),
      trailing:
      Icon(Icons.file_download, color: Colors.white, size: 30.0),
      onTap: () async{
        await launch(NetworkUtil.ASSETS_FILE_URL+download.upload_file);
      },
    );

    Card makeCard(Download download) => Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
        child: makeListTile(download),
      ),
    );

    final makeBody = Container(
      // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: (downloads!=null?downloads.length:0),
        itemBuilder: (BuildContext context, int index) {
          return makeCard(downloads[index]);
        },
      ),
    );

    final makeBottom = Container(
      height: 55.0,
      child: BottomAppBar(
        color: Color.fromRGBO(58, 66, 86, 1.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.home, color: Colors.white),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.blur_on, color: Colors.white),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.hotel, color: Colors.white),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.account_box, color: Colors.white),
              onPressed: () {},
            )
          ],
        ),
      ),
    );
    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );

    return Scaffold(
      backgroundColor: DesignCourseAppTheme.grey,
      appBar: topAppBar,
      body: (_isShowing? Center(child: CircularProgressIndicator()):downloads.length>0?makeBody:Center(child: Text("Tidak ada data"))),
    );
  }
}



