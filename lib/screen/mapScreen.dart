import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';
import 'package:elenpa/model/intake.dart';
import 'dart:core';

class MapScreen extends StatefulWidget {
  final String title ="Map";
  @override
  MapScreenState createState() => MapScreenState();
}

class MapScreenState extends State<MapScreen> {
  
  NetworkUtil _netUtil = new NetworkUtil();
  bool _isShowing = false;
  List<DropdownMenuItem<int>> intakeList = [];
  String MarkerEdit = "";
  var defaultLat = -6.913973;
  var defaultLon = 107.609001;
  int _selectedIntake = null;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController mapController;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  List intake=[];
  @override
  void initState() {
    getPins("");
    loadIntakeList();
    super.initState();
  }
  double zoomVal=5.0;

  @override
  Widget build(BuildContext context) {
    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );
    return Scaffold(
      appBar: topAppBar,
      body: Stack(
        children: <Widget>[
          _buildGoogleMap(context),
          _buildIntakeList(),
        ],
      ),
    );
  }

Future<void> loadIntakeList() async{
    show();
    intakeList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    return _netUtil.get(NetworkUtil.INTAKE_URL,headers).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        dismiss();
        return null;
      } else {
        setState(() {
          print(res['data']);
          intakeList.add(new DropdownMenuItem(
            child: new Text("Semua"),
            value: 0,
          ));
          res['data'].map((model) => intakeList.add(new DropdownMenuItem(
            child: new Text(model['sumber_mata_air']),
            value: int.parse((model['intake_id']!=null?model['intake_id']:0)),
          ))).toList();
        });
        dismiss();
      }
    });
  }

Widget _buildIntakeList(){
return Container(
            margin: new EdgeInsets.symmetric(
              horizontal: 10.0, vertical: 20.0),
                      child: DropdownButton(
                        hint: new Text('Pilih Intake'),
                        items: intakeList,
                        value: _selectedIntake,
                        onChanged: (value) {
                          setState(() {
                            _selectedIntake = value;
                          viewMarkers(value.toString());
                          });
                        },
                        isExpanded: true,
                      ),

                    )
                    ;
}
Future<void> viewMarkers(String id) async{
  markers.clear();
  getPins(id);
}
Future<bool> updateLocation(String intake_id, lat, lng) async{
    show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String user_roles = prefs.getString("user_roles");
      print("intake_id");
      print(intake_id);
      return _netUtil.post(NetworkUtil.UPDATE_INTAKE_URL, body: {
        "intake_id": intake_id,
        "lat": lat,
        "lng": lng
      }).then((dynamic res) async {
        if (res['status'] == 400) {
          dismiss();
          return false;
        }
        else {
          print("update location success");
          dismiss();
          return true;
        }
      });
  }
  Widget _zoomminusfunction() {

    return Align(
      alignment: Alignment.topLeft,
      child: IconButton(
          icon: Icon(FontAwesomeIcons.searchMinus,color:DesignCourseAppTheme.grey),
          onPressed: () {
            zoomVal--;
            _minus( zoomVal);
          }),
    );
  }
  Widget _zoomplusfunction() {

    return Align(
      alignment: Alignment.topRight,
      child: IconButton(
          icon: Icon(FontAwesomeIcons.searchPlus,color:DesignCourseAppTheme.grey),
          onPressed: () {
            zoomVal++;
            _plus(zoomVal);
          }),
    );
  }

  Future<void> _minus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: LatLng(defaultLat, defaultLon), zoom: zoomVal)));
  }
  Future<void> _plus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: LatLng(defaultLat, defaultLon), zoom: zoomVal)));
  }


  Widget _buildContainer() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
                margin: const EdgeInsets.all(10.0),
        height: 150.0,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://lh5.googleusercontent.com/p/AF1QipO3VPL9m-b355xWeg4MXmOQTauFAEkavSluTtJU=w225-h160-k-no",
                  40.738380, -73.988426,"Gramercy Tavern"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://lh5.googleusercontent.com/p/AF1QipMKRN-1zTYMUVPrH-CcKzfTo6Nai7wdL7D8PMkt=w340-h160-k-no",
                  40.761421, -73.981667,"Le Bernardin"),
            ),
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(
                  "https://images.unsplash.com/photo-1504940892017-d23b9053d5d4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
                  40.732128, -73.999619,"Blue Hill"),
            ),
          ],
        ),
      ),
    );
  }

  Widget _boxes(String _image, double lat,double long,String restaurantName) {
    return  GestureDetector(
      onTap: () {
        _gotoLocation(lat,long);
      },
      child:Container(
        child: new FittedBox(
          child: Material(
              color: Colors.white,
              elevation: 14.0,
              borderRadius: BorderRadius.circular(24.0),
              shadowColor: Color(0x802196F3),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 180,
                    height: 200,
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(24.0),
                      child: Image(
                        fit: BoxFit.fill,
                        image: NetworkImage(_image),
                      ),
                    ),),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: myDetailsContainer1(restaurantName),
                    ),
                  ),

                ],)
          ),
        ),
      ),
    );
  }

  Widget myDetailsContainer1(String restaurantName) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Container(
              child: Text(restaurantName,
                style: TextStyle(
                    color: Color(0xff6200ee),
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold),
              )),
        ),
        SizedBox(height:5.0),
        Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                    child: Text(
                      "4.1",
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 18.0,
                      ),
                    )),
                Container(
                  child: Icon(
                    FontAwesomeIcons.solidStar,
                    color: Colors.amber,
                    size: 15.0,
                  ),
                ),
                Container(
                  child: Icon(
                    FontAwesomeIcons.solidStar,
                    color: Colors.amber,
                    size: 15.0,
                  ),
                ),
                Container(
                  child: Icon(
                    FontAwesomeIcons.solidStar,
                    color: Colors.amber,
                    size: 15.0,
                  ),
                ),
                Container(
                  child: Icon(
                    FontAwesomeIcons.solidStar,
                    color: Colors.amber,
                    size: 15.0,
                  ),
                ),
                Container(
                  child: Icon(
                    FontAwesomeIcons.solidStarHalf,
                    color: Colors.amber,
                    size: 15.0,
                  ),
                ),
                Container(
                    child: Text(
                      "(946)",
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 18.0,
                      ),
                    )),
              ],
            )),
        SizedBox(height:5.0),
        Container(
            child: Text(
              "American \u00B7 \u0024\u0024 \u00B7 1.6 mi",
              style: TextStyle(
                color: Colors.black54,
                fontSize: 18.0,
              ),
            )),
        SizedBox(height:5.0),
        Container(
            child: Text(
              "Closed \u00B7 Opens 17:00 Thu",
              style: TextStyle(
                  color: Colors.black54,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold),
            )),
      ],
    );
  }

  Widget _buildGoogleMap(BuildContext context) {
    return (_isShowing? Center(child: CircularProgressIndicator()):Container(
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 100.0),
      height: MediaQuery.of(context).size.height/1.2,
      width: MediaQuery.of(context).size.width,
      child: GoogleMap(
        mapType: MapType.normal,
        
        onCameraMove: ((_position) => _updatePosition(_position)),
        initialCameraPosition:  CameraPosition(target: LatLng(defaultLat, defaultLon), zoom: 7),
        markers: Set<Marker>.of(markers.values),
      ),
    ));
  }
void _updatePosition(CameraPosition _position) {
    print(
        'inside updatePosition ${_position.target.latitude} ${_position.target.longitude}');
    //post update lat lon
updateLocation(MarkerEdit,_position.target.latitude, _position.target.longitude);
  }

  Future<void> _gotoLocation(double lat,double long) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: LatLng(lat, long), zoom: 15,tilt: 50.0,
      bearing: 45.0,)));
  }

  Future<void> getPins(String intake_id) async {
    show();
    NetworkUtil _netUtil = new NetworkUtil();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    Map<String, String> headers = {"token": token};
    _netUtil.get(NetworkUtil.INTAKE_URL,headers).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get data");
        return null;
      } else {
        setState(() {
          print("sukses");
          res['data'].forEach((item) {
            if(item['lat']!="" && item['lng']!="") {
              if(item['lat']!=null && item['lng']!=null) {
                
              if(intake_id=="" || intake_id=="0"){
                var markerIdVal = item['sumber_mata_air'];
                print(markerIdVal);
                final MarkerId markerId = MarkerId(markerIdVal);

                // creating a new MARKER
                final Marker marker = Marker(
                  markerId: markerId,
                  draggable: true,
                  position: LatLng(
                    double.parse(item['lat']),
                    double.parse(item['lng']),
                  ),
                  infoWindow: InfoWindow(
                      title: item['sumber_mata_air']),
                  icon: BitmapDescriptor.defaultMarkerWithHue(
                    BitmapDescriptor.hueRed,
                  ),
                );

                setState(() {
                  markers[markerId] = marker;
                });
              } else {
                if(intake_id==item['intake_id']){
                var markerIdVal = item['intake_id'];
                MarkerEdit = item['intake_id'];
                print(markerIdVal);
                final MarkerId markerId = MarkerId(markerIdVal);

                // creating a new MARKER
                final Marker marker = Marker(
                  markerId: markerId,
                  draggable: true,
                  position: LatLng(
                    double.parse(item['lat']),
                    double.parse(item['lng']),
                  ),
                  infoWindow: InfoWindow(
                      title: item['sumber_mata_air']),
                  icon: BitmapDescriptor.defaultMarkerWithHue(
                    BitmapDescriptor.hueRed,
                  ),
                );

                setState(() {
                  markers[markerId] = marker;
                });
                }
              }
              } else {
                var markerIdVal = item['sumber_mata_air'];
                print(markerIdVal);
                final MarkerId markerId = MarkerId(markerIdVal);

                // creating a new MARKER
                final Marker marker = Marker(
                  markerId: markerId,
                  draggable: true,
                  position: LatLng(
                    defaultLat,
                    defaultLon,
                  ),
                  infoWindow: InfoWindow(
                      title: item['sumber_mata_air']),
                  icon: BitmapDescriptor.defaultMarkerWithHue(
                    BitmapDescriptor.hueRed,
                  ),
                );

                setState(() {
                  markers[markerId] = marker;
                });
              }
            } else {
                var markerIdVal = item['sumber_mata_air'];
                print(markerIdVal);
                final MarkerId markerId = MarkerId(markerIdVal);

                // creating a new MARKER
                final Marker marker = Marker(
                  markerId: markerId,
                  draggable: true,
                  position: LatLng(
                    defaultLat,
                    defaultLon,
                  ),
                  infoWindow: InfoWindow(
                      title: item['sumber_mata_air']),
                  icon: BitmapDescriptor.defaultMarkerWithHue(
                    BitmapDescriptor.hueRed,
                  ),
                );

                setState(() {
                  markers[markerId] = marker;
                });
              }
          });
        });
        dismiss();
      }
    });
  }

  Future<void> Add(Map<String, dynamic> json)async{
    var markerIdVal = json['sumber_mata_air'];
    print(markerIdVal);
    final MarkerId markerId = MarkerId(markerIdVal);

    // creating a new MARKER
    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(
        json['lat'],
        json['lon'],
      ),
      infoWindow: InfoWindow(title: json['sumber_mata_air'], snippet: '*'),
      icon: BitmapDescriptor.defaultMarkerWithHue(
        BitmapDescriptor.hueRed,
      ),
    );

    setState(() {
      markers[markerId] = marker;
    });
  }
}



Marker gramercyMarker = Marker(
  markerId: MarkerId('gramercy'),
  position: LatLng(40.738380, -73.988426),
  infoWindow: InfoWindow(title: 'Gramercy Tavern'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueRed,
  ),
);

Marker bernardinMarker = Marker(
  markerId: MarkerId('bernardin'),
  position: LatLng(40.761421, -73.981667),
  infoWindow: InfoWindow(title: 'Le Bernardin'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueRed,
  ),
);
Marker blueMarker = Marker(
  markerId: MarkerId('bluehill'),
  position: LatLng(40.732128, -73.999619),
  infoWindow: InfoWindow(title: 'Blue Hill'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueRed,
  ),
);

//New York Marker

Marker newyork1Marker = Marker(
  markerId: MarkerId('newyork1'),
  position: LatLng(40.742451, -74.005959),
  infoWindow: InfoWindow(title: 'Los Tacos'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueRed,
  ),
);
Marker newyork2Marker = Marker(
  markerId: MarkerId('newyork2'),
  position: LatLng(40.729640, -73.983510),
  infoWindow: InfoWindow(title: 'Tree Bistro'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueRed,
  ),
);
Marker newyork3Marker = Marker(
  markerId: MarkerId('newyork3'),
  position: LatLng(40.719109, -74.000183),
  infoWindow: InfoWindow(title: 'Le Coucou'),
  icon: BitmapDescriptor.defaultMarkerWithHue(
    BitmapDescriptor.hueRed,
  ),
);