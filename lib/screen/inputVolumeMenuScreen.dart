import 'package:flutter/material.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';

class InputVolumeMenuScreen extends StatefulWidget {


  String title = "Input Volume";
  @override
  _InputVolumeMenuScreenState createState() => new _InputVolumeMenuScreenState();
}

class _InputVolumeMenuScreenState extends State<InputVolumeMenuScreen> {
  bool _isShowing = false;
  NetworkUtil _netUtil = new NetworkUtil();
  dynamic User = {"nama_lengkap":"", "email":""};
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    final makeBody = Container(
      // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
      child: ListView(
        children:<Widget>[
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
              child: ListTile(
                contentPadding:
                EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                title: Text(
                  "Input Dengan OCR",
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                ),
                trailing:
                Icon(Icons.add_a_photo, color: Colors.white, size: 30.0),
                onTap: () {
                  //redirect
                  Navigator.pushNamed(context, '/InputVolumeScreen');
                },
              ),
            ),
          ),
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
              child: ListTile(
                contentPadding:
                EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                title: Text(
                  "Input Tanpa OCR",
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                ),
                trailing:
                Icon(Icons.add, color: Colors.white, size: 30.0),
                onTap: () {
                  //redirect
                  Navigator.pushNamed(context, '/InputVolumeManualScreen');
                },
              ),
            ),
          )
        ]
      ),
    );

    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );

    return Scaffold(
      backgroundColor: DesignCourseAppTheme.grey,
      appBar: topAppBar,
      body: (_isShowing? Center(child: CircularProgressIndicator()):makeBody),
    );
  }
}



