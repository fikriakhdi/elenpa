import 'package:elenpa/model/pemakaian.dart';
import 'package:elenpa/utils/network_util.dart';
import 'package:flutter/material.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
class DetailPemakaianScreen extends StatefulWidget {
//  ProfileScreen({Key key, this.title}) : super(key: key);

  final String title ="Detail Pemakaian";

  @override
  _DetailPemakaianScreenState createState() => new _DetailPemakaianScreenState();
}
final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
class _DetailPemakaianScreenState extends State<DetailPemakaianScreen> {
  var  pemakaian = {};
  bool edit = false;
  NetworkUtil _netUtil = new NetworkUtil();
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  Future<void> getPemakaianData() async{

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    String pemakaian_id = prefs.getString('edit_pemakaian');
    String user_roles = prefs.getString('user_roles');
    print("PEMAKAIAN ID");
    print(pemakaian_id);
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"pemakaian_id": pemakaian_id};
    return _netUtil.post(NetworkUtil.USAGE_SINGLE_URL, headers:headers, body:body).then((
        dynamic res) async {
      if (res['status'] == 400) {
        print("Error to get data");
        dismiss();
      } else{
        print(res['data']);
        setState(() {
          if(user_roles=="Perusahaan") edit = false;
          else edit=true;
          pemakaian =res['data'];
        dismiss();
        });
        
      }
    });
  }
  @override
  void initState() {
    show();
    getPemakaianData();
//  downloads = getLessons();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final levelIndicator = Container(
      child: Container(
      ),
    );




    return Scaffold(

      body: (_isShowing? Center(child: CircularProgressIndicator()):Column(
        children: <Widget>[Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.48,
          padding: EdgeInsets.all(40.0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, .9)),
          child: Center(
            child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.info,
          color: Colors.white,
          size: 40.0,
        ),
        Container(
          width: 40.0,
          child: new Divider(color: Colors.white),
        ),
        SizedBox(height: 10.0),
        Text(
          "Detail Pemakaian",
          style: TextStyle(color: Colors.white, fontSize: 20.0),
        ),
        SizedBox(height: 20.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
                flex: 10,
                child: Padding(
                    padding: EdgeInsets.only(left: 0.0),
                   child: Text(
                     pemakaian['nama_lengkap'],
                    //  pemakaian.nama_pemakaian,
                     style: TextStyle(color: Colors.white, fontSize: 23.0),
                   )
                ))
          ],
        ),SizedBox(height: 10.0),Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            RaisedButton(
          onPressed: ()async{
        await launch(NetworkUtil.ASSETS_FILE_URL+pemakaian['photo_meteran']);
      },
          color: Colors.white,
          child:
          Text("Foto"),
        ),SizedBox(width: 10.0),pemakaian['upload_berita_acara']!=""?RaisedButton(
          onPressed: ()async{
        await launch(NetworkUtil.ASSETS_FILE_URL+pemakaian['upload_file']);
      },
          color: Colors.white,
          child:
          Text("Berita Acara"),
        ):Text("")
          ],
        ),
      ],
    ),
          ),
        ),
        Positioned(
          left: 8.0,
          top: 60.0,
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back, color: Colors.white),
          ),
        )
      ],
    ),Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(10.0),
      child: Container(
        child: Column(
          children: <Widget>[
            Text("Nomor Sippa :"),
            Text(pemakaian['nomor_sippa'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),),
            SizedBox(height: 5.0),Text("PPPD :"),
            Text(pemakaian['nama_pppd'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold)),
            SizedBox(height: 5.0),Text("Sumber Mata Air :"),
            Text(pemakaian['sumber_mata_air'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),),
            SizedBox(height: 5.0),Text("Volume Meter :"),
            Text(pemakaian['volume_meter'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),),
            SizedBox(height: 5.0),Text("NPA :"),
            Text(pemakaian['npa'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),),
            SizedBox(height: 5.0),Text("Pajak :"),
            Text("Rp."+pemakaian['pajak'],style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),), 
            edit?Container(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        width: MediaQuery.of(context).size.width,
        child: RaisedButton(
          onPressed: () => {
            editCompany()
          },
          color: Color.fromRGBO(58, 66, 86, 1.0),
          child:
          Text("Ubah Data", style: TextStyle(color: Colors.white)),
        )):Text("")],
        ),
      ),
    )],
      )),
    );
  }
  Future<void> editCompany() async{
    Navigator.pushNamed(
        context, '/editPemakaianScreen'
    );
  }
}
