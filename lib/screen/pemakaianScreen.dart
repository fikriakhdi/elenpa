import 'package:flutter/material.dart';
import 'package:elenpa/model/pemakaian.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:loadmore/loadmore.dart';
import 'dart:async';

class PemakaianScreen extends StatefulWidget {
  PemakaianScreen() : super();

  final String title = "Rekap Pemakaian";

  @override
  PemakaianScreenState createState() => PemakaianScreenState();
}

class PemakaianScreenState extends State<PemakaianScreen> {
  bool _isShowing = false;
  bool edit = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  List pemakaian=[];
  List selectedPemakaian;
  bool sort;

  int total = 100;
  int start = 0;
  int length = 10;
  int get count => pemakaian.length;
  Future<void> getPemakaian() async{
    NetworkUtil _netUtil = new NetworkUtil();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    String user_roles = prefs.getString('user_roles');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"start": start.toString(), "length":length.toString()};
    _netUtil.post(NetworkUtil.USAGE_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get data");
        return null;
      } else {
//        print(res['Messages']);
        setState(() {
          if(user_roles=="Perusahaan") edit = false;
          else edit=true;
          start += 10;
          total = res['data'][0]['total_row'];
          pemakaian += res['data'].map((model) => Pemakaian.fromJson(model)).toList();
        });
        dismiss();
      }
    });
  }
  @override
  void initState() {
    show();
    sort = false;
    selectedPemakaian = [];
    getPemakaian();
    super.initState();
  }

  onSortColum(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        pemakaian.sort((a, b) => a.nama_lengkap.compareTo(b.nama_lengkap));
      } else {
        pemakaian.sort((a, b) => b.nama_lengkap.compareTo(a.nama_lengkap));
      }
    }
  }




  @override
  Widget build(BuildContext context) {

    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );
    return Scaffold(
      appBar: topAppBar,
        body: (_isShowing? Center(child: CircularProgressIndicator()):Container(
          child: RefreshIndicator(
            child: LoadMore(
              isFinish: count >= total,
              onLoadMore: _loadMore,
              child: ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(
//                              leading: const Icon(Icons.data_usage),
                              title: Text('${pemakaian[index].nama_lengkap}',style: TextStyle(fontWeight: FontWeight.bold)),
                              subtitle: Text('PPD: ${pemakaian[index].nama_pppd} \n${pemakaian[index].sumber_mata_air}\nTanggal: ${pemakaian[index].tanggal}'),
                              contentPadding: EdgeInsets.all(20.0),
                              dense: false,
                              isThreeLine: true,
                              trailing: Icon(FontAwesomeIcons.water),
                              onTap: (){
                                viewUsage(pemakaian[index].id);
                              },
                            ),
                            new ButtonTheme.bar( // make buttons use the appropriate styles for cards
                                child: new Column(
                                    children: <Widget>[
                                      new Text('NPA: '+pemakaian[index].npa, style: TextStyle(fontSize: 15),),
                                      new Text('Volume: '+pemakaian[index].volume_meter, style: TextStyle(fontSize: 15)),
                                SizedBox(
                                    width: 10,
                                    height: 10,)
                                    ]))
                          ]));
                },
                itemCount: count,
              ),
              whenEmptyLoad: false,
              delegate: DefaultLoadMoreDelegate(),
              textBuilder: DefaultLoadMoreTextBuilder.english,
            ),
            onRefresh: _refresh,
          ),
        )),
    );
  }
  Future<bool> _loadMore() async {
    print("onLoadMore");
    await Future.delayed(Duration(seconds: 0, milliseconds: 100));
    getPemakaian();
    return true;
  }

  Future<void> viewUsage(String id_pemakaian) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('edit_pemakaian', id_pemakaian);
    Navigator.pushNamed(
        context, '/detailPemakaianScreen'
    );
  }

  Future<void> _refresh() async {
    print("refresh");
    await Future.delayed(Duration(seconds: 0, milliseconds: 2000));
    pemakaian.clear();
    setState(() {
      start = 0;
      length = 10;
    });
    getPemakaian();
  }
}

