import 'package:elenpa/screen/homeScreen.dart';
import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:elenpa/model/user.dart';
import 'dart:async';
import 'package:http/http.dart' show Client;
import 'package:elenpa/utils/network_util.dart';
import 'package:elenpa/model/user.dart';
import 'package:elenpa/model/session.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileScreen extends StatefulWidget {
//  ProfileScreen({Key key, this.title}) : super(key: key);

  final String title ="My Profile";

  @override
  _ProfileScreenState createState() => new _ProfileScreenState();
}
final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
class _ProfileScreenState extends State<ProfileScreen> {
  NetworkUtil _netUtil = new NetworkUtil();
  dynamic User = {"nama_lengkap":"", "email":""};
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  Future<bool> signout() async{
    show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    return _netUtil.get(NetworkUtil.LOGOUT_URL,headers).then((dynamic res) async{
      if(res['status']==400) {
        return false;
      }
      else {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("userId", "");
        prefs.setString("P3NPA_PIN", "");
        dismiss();
        return true;
      }
    });
  }

  Future<void> getProfileData() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
      Map<String, String> headers = {"token": Token};
      return _netUtil.get(NetworkUtil.PROFILE_URL, headers).then((
          dynamic res) async {
        if (res['status'] == 400) {
          print("Error to get profile data");
        } else{
          setState(() {
            User = res['data'];
          });
          dismiss();
        }
      });
  }
  @override
  void initState() {
    show();
    getProfileData();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );
    return new Scaffold(
        appBar: topAppBar,
        body: (_isShowing? Center(child: CircularProgressIndicator()):new Stack(
          children: <Widget>[
            ClipPath(
              child: Container(color: DesignCourseAppTheme.grey),
              clipper: getClipper(),
            ),
            Positioned(
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                top: MediaQuery
                    .of(context)
                    .size
                    .height / 6,
                child: Column(
                  children: <Widget>[
                    new Container(
                        width: 150.0,
                        height: 150.0,
                        child: FadeInImage.assetNetwork(
                            placeholder: 'assets/images/user.png',
                            image:NetworkUtil.ASSETS_FILE_URL+User['photo_user']
                        )),
                    SizedBox(height: 30.0),
                    Text(
                      User['nama_lengkap'],
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat'),
                    ),
                    SizedBox(height: 15.0),
                    Text(

                      User['email'],
                      style: TextStyle(
                          fontSize: 20.0,
                          fontStyle: FontStyle.italic,
                          fontFamily: 'Montserrat'),
                    ),
                    SizedBox(height: 15.0),
                    Container(
                        height: 45.0,
                        width: 115.0,
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          shadowColor: Colors.greenAccent,
                          color: Colors.green,
                          elevation: 7.0,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).pushNamed('/EditProfileScreen');
                            },
                            child: Center(
                              child: Text(
                                'EDIT PROFILE',
                                style: TextStyle(color: Colors.white,
                                    fontSize: 15.0,
                                    fontFamily: 'Montserrat'),
                              ),
                            ),
                          ),
                        )),
                    SizedBox(height: 15.0),
                    Container(
                        height: 45.0,
                        width: 115.0,
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          shadowColor: Colors.redAccent,
                          color: Colors.red,
                          elevation: 7.0,
                          child: GestureDetector(
                            onTap: () async{
                              signout().then((isSuccess) {
                              if(isSuccess){
                                Navigator.of(context)
                                    .pushNamedAndRemoveUntil('/LoginScreen', (Route<dynamic> route) => false);
                              } else {
                                _scaffoldState.currentState.showSnackBar(
                                  SnackBar(
                                    content: Text("Falied to Logout"),
                                  ),
                                );
                              }
                              });
                              },
                            child: Center(
                              child: Text(
                                'LOG OUT',
                                style: TextStyle(color: Colors.white,
                                    fontSize: 15.0,
                                    fontFamily: 'Montserrat'),
                              ),
                            ),
                          ),
                        ))
                  ],
                ))
          ],
        )));
  }

}


class getClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();

    path.lineTo(0.0, size.height / 1.9);
    path.lineTo(size.width + 125, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}