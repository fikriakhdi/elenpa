import 'package:flutter/material.dart';
import 'package:elenpa/model/perusahaan.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:loadmore/loadmore.dart';
import 'dart:async';

class CompanyScreen extends StatefulWidget {
  CompanyScreen() : super();

  final String title = "Data Perusahaan";

  @override
  CompanyScreenState createState() => CompanyScreenState();
}

class CompanyScreenState extends State<CompanyScreen> {
  List perusahaan=[];
  bool sort;
  bool _isShowing = false;
  bool edit = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  int total = 100;
  int start = 0;
  int length = 10;
  int get count => perusahaan.length;
  Future<void> getUser() async{
    NetworkUtil _netUtil = new NetworkUtil();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    String user_roles = prefs.getString('user_roles');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"start": start.toString(), "length":length.toString()};
    _netUtil.post(NetworkUtil.COMPANY_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        dismiss();
        return null;
      } else {
        setState(() {
          print("ANJING");
          print(res);
          if(user_roles=="Dinas") edit=true;
          else edit=false;
          if( res['data'].length>0){
          start += 10;
          total = res['data'][0]['total_row'];
          perusahaan += res['data'].map((model) => Perusahaan.fromJson(model)).toList();
          }
        });
        dismiss();
      }
    });
  }
  @override
  void initState() {
    show();
    sort = false;
    getUser();
    super.initState();
  }

  onSortColum(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        perusahaan.sort((a, b) => a.nama_lengkap.compareTo(b.nama_lengkap));
      } else {
        perusahaan.sort((a, b) => b.nama_lengkap.compareTo(a.nama_lengkap));
      }
    }
  }




  @override
  Widget build(BuildContext context) {

    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );
    return Scaffold(
      appBar: topAppBar,
      body: (_isShowing? Center(child: CircularProgressIndicator()):Container(
        child: RefreshIndicator(
          child: LoadMore(
            isFinish: count >= total,
            onLoadMore: _loadMore,
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    elevation: 4.0,
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
//                              leading: const Icon(Icons.data_usage),
                            title: Text('${perusahaan[index].nama_perusahaan}',style: TextStyle(fontWeight: FontWeight.bold)),
                            subtitle: Text('Provinsi: ${perusahaan[index].nama_provinsi} \nKota:${perusahaan[index].nama_kota}'),
                            contentPadding: EdgeInsets.all(20.0),
                            dense: false,
                            isThreeLine: true,
                            trailing: Icon(FontAwesomeIcons.building),
                            onTap:(){
                              viewCompany(perusahaan[index].id_perusahaan);
                            }
                          )
                        ]));
              },
              itemCount: count,
            ),
            whenEmptyLoad: false,
            delegate: DefaultLoadMoreDelegate(),
            textBuilder: DefaultLoadMoreTextBuilder.english,
          ),
          onRefresh: _refresh,
        ),
      )),
    );
  }
  Future<bool> _loadMore() async {
    print("onLoadMore");
    await Future.delayed(Duration(seconds: 0, milliseconds: 100));
    getUser();
    return true;
  }

Future<void> viewCompany(String id_perusahaan) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('edit_perusahaan', id_perusahaan);
    Navigator.pushNamed(
        context, '/detailPerusahaanScreen'
    );
  }
  Future<void> _refresh() async {
    print("refresh");
    await Future.delayed(Duration(seconds: 0, milliseconds: 2000));
    perusahaan.clear();
    setState(() {
      start = 0;
      length = 10;
    });
    getUser();
  }
}

