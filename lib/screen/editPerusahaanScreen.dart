
import 'package:flutter/material.dart';
import 'package:elenpa/theme/theme.dart';
import 'dart:async';
import 'package:elenpa/utils/network_util.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditPerusahaanScreen extends StatefulWidget {
//  ProfileScreen({Key key, this.title}) : super(key: key);

  final String title ="Edit Perusahaan";

  @override
  _EditPerusahaanScreenState createState() => new _EditPerusahaanScreenState();
}
final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
class _EditPerusahaanScreenState extends State<EditPerusahaanScreen> {
  NetworkUtil _netUtil = new NetworkUtil();
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  TextEditingController nama_lengkap = new TextEditingController();
  TextEditingController alamat = new TextEditingController();
  TextEditingController notelp = new TextEditingController();
  TextEditingController email = new TextEditingController();
  int _selectedProvince = null;
  int _selectedCity = null;
  dynamic User = {"nama_lengkap":"", "email":""};
  List<DropdownMenuItem<int>> provinceList = [];
  List<DropdownMenuItem<int>> cityList = [];

  Future<void> loadProvinceList() async{
    show();
    provinceList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    return _netUtil.get(NetworkUtil.PROVINCE_URL,headers).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        dismiss();
        return null;
      } else {
        setState(() {
          print(res['data']);
          res['data'].map((model) => provinceList.add(new DropdownMenuItem(
            child: new Text(model['nama_provinsi']),
            value: int.parse((model['provinsi_id']!=null?model['provinsi_id']:0)),
          ))).toList();
        });
        dismiss();
      }
    });
  }

  Future<void> loadCityList(String provinsi_id) async{
//    show();
    cityList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"provinsi_id": provinsi_id};
    return _netUtil.post(NetworkUtil.CITY_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        dismiss();
        return null;
      } else {
        setState(() {
          print(res['data']);
          res['data'].map((model) => cityList.add(new DropdownMenuItem(
            child: new Text(model['nama_kota']),
            value: int.parse(model['kota_id']),
          ))).toList();
          _selectedCity = int.parse(res['data'][0]['kota_id']);
        });
      }
    });
  }

  Future<bool> updateProfile() async{
    cityList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    String edit_perusahaan = prefs.getString('edit_perusahaan');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"user_id":edit_perusahaan,"nama_lengkap": nama_lengkap.text,"email":email.text, "provinsi_id":_selectedProvince.toString(), "kota_id":_selectedCity.toString(), "alamat":alamat.text,"nomor_telp":notelp.text};
    print(body);
    return _netUtil.post(NetworkUtil.UPDATE_COMPANY_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        dismiss();
        return null;
      } else {
        getProfileData();
        dismiss();
        return true;
      }
    });
  }

  Future<void> setCityList(String provinsi_id) async{
    show();
    cityList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"provinsi_id": provinsi_id};
    return _netUtil.post(NetworkUtil.CITY_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        dismiss();
        return null;
      } else {
        setState(() {
          res['data'].map((model) => cityList.add(new DropdownMenuItem(
            child: new Text(model['nama_kota']),
            value: int.parse(model['kota_id']),
          ))).toList();
          _selectedCity = int.parse(res['data'][0]['kota_id']);
        });
      }
    });
  }
  Future<bool> signout() async{
    show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    return _netUtil.get(NetworkUtil.LOGOUT_URL,headers).then((dynamic res) async{
      if(res['status']==400) {
        return false;
      }
      else {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("userId", "");
        prefs.setString("P3NPA_PIN", "");
        dismiss();
        return true;
      }
    });
  }

  Future<void> getProfileData() async{

    show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    String user_id = prefs.getString('edit_perusahaan');
    print("USER ID NYA");
    print(user_id);
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"user_id": user_id};
    return _netUtil.post(NetworkUtil.SINGLE_USER, headers:headers, body:body).then((
        dynamic res) async {
      if (res['status'] == 400) {
        print("Error to get data");
        dismiss();
      } else{
        setState(() {
          User = res['data'];
          nama_lengkap.text = User['nama_lengkap'];
          alamat.text = User['alamat'];
          notelp.text = User['nomor_telp'];
          email.text = User['email'];
          _selectedProvince = int.parse(User['provinsi_id']);
          setCityList(User['provinsi_id']).then((ret){
            _selectedCity = int.parse(User['kota_id']);
          });
        });
        
        dismiss();
      }
    });
  }
  @override
  void initState() {
    show();
    getProfileData();
    loadProvinceList();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {



    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );
    return new Scaffold(
        key: _scaffoldState,
        appBar: topAppBar,
        body: (_isShowing? Center(child: CircularProgressIndicator()):new ListView(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 20.0),
                    TextFormField(
                      controller:nama_lengkap,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Tolong Masukkan Nama lengkap Anda';
                        }
                        return null;
                      },

                      decoration: new InputDecoration(hintText: "Nama Lengkap", contentPadding: const EdgeInsets.all(20.0)),
                    ),
                    TextFormField(
                      controller:email,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Tolong Masukkan Email Anda';
                        }
                        return null;
                      },

                      decoration: new InputDecoration(hintText: "Email", contentPadding: const EdgeInsets.all(20.0)),
                    ),
                    TextFormField(
                      controller:notelp,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Tolong Masukkan No. Telp Anda';
                        }
                        return null;
                      },

                      decoration: new InputDecoration(hintText: "Nomor Telepon", contentPadding: const EdgeInsets.all(20.0)),
                    ),
                    TextFormField(
                      controller:alamat,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Tolong Masukkan Alamat Anda';
                        }
                        return null;
                      },

                      decoration: new InputDecoration(hintText: "Alamat", contentPadding: const EdgeInsets.all(20.0)),
                    ),
                    SizedBox(height: 15.0),
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: DropdownButton(
                      hint: new Text('Pilih Provinsi'),
                      items: provinceList,
                      value: _selectedProvince,
                      onChanged: (value) {
                        setState(() {
                          _selectedProvince = value;
                          loadCityList(value.toString());
                        });
                      },
                      isExpanded: true,
                    ),

                    ),
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: DropdownButton(
                        hint: new Text('Pilih Kota'),
                        items: cityList,
                        value: _selectedCity,
                        onChanged: (value) {
                          setState(() {
                            _selectedCity = value;
                          });
                        },
                        isExpanded: true,
                      ),

                    ),
                    Container(
                        height: 45.0,
                        width: 115.0,
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          shadowColor: Colors.greenAccent,
                          color: Colors.green,
                          elevation: 7.0,
                          child: GestureDetector(
                            onTap: () {
                              show();
                              updateProfile().then((isSuccess) {
                                if(isSuccess){
                              _scaffoldState.currentState.showSnackBar(
                                SnackBar(
                                  content: Text("Edit Sukses"),
                                ),
                              );
                              } else{
                              _scaffoldState.currentState.showSnackBar(
                                SnackBar(
                                  content: Text("Edit Gagal"),
                                ),
                              );
                              }
                            });
                            },
                            child: Center(
                              child: Text(
                                'SIMPAN',
                                style: TextStyle(color: Colors.white,
                                    fontSize: 15.0,
                                    fontFamily: 'Montserrat'),
                              ),
                            ),
                          ),
                        )),
                  ],
                ))
          ],
        )));
  }

}


class getClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();

    path.lineTo(0.0, size.height / 1.9);
    path.lineTo(size.width + 125, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}