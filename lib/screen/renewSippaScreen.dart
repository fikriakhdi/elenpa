import 'package:flutter/material.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';
import 'dart:async';
class RenewSippaScreen extends StatefulWidget {


  String title = "Pembaruan SIPPA";
  @override
  _RenewSippaScreenState createState() => new _RenewSippaScreenState();
}

class _RenewSippaScreenState extends State<RenewSippaScreen> {
  bool _isShowing = false;
  NetworkUtil _netUtil = new NetworkUtil();
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    final makeBody = Container(
      // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
      child: ListView(
        children:<Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text("PENGAJUAN SIPPA :", style:TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold)),
          ),
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
              child: ListTile(
                contentPadding:
                EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                title: Text(
                  "Kewenangan Provinsi Jawa Barat",
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                ),
                trailing:
                Icon(Icons.arrow_right, color: Colors.white, size: 30.0),
                onTap: () async{
                  //redirect
                  await launch(NetworkUtil.KEWENANGAN_PROVINSI_JABAR_URL);
                },
              ),
            ),
          ),
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
              child: ListTile(
                contentPadding:
                EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                title: Text(
                  "Kewenangan Pusat/Kementerian PU PR ",
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                ),
                trailing:
                Icon(Icons.arrow_right, color: Colors.white, size: 30.0),
                onTap: () async{
                  //redirect
                  await launch(NetworkUtil.KEWENANGAN_PUSAT_URL);
                },
              ),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text("PERSYARATAN :", style:TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold)),
          ),
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
              child: ListTile(
                contentPadding:
                EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                title: Text(
                  "Persyaratan Kewenangan Jawa Barat",
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                ),
                trailing:
                Icon(Icons.arrow_right, color: Colors.white, size: 30.0),
                onTap: () async{
                  //redirect
                  await launch(NetworkUtil.PERSYARATAN_JABAR_URL);
                },
              ),
            ),
          ),
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
              child: ListTile(
                contentPadding:
                EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                title: Text(
                  "Persyaratan Kewenangan Pusat",
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                ),
                trailing:
                Icon(Icons.arrow_right, color: Colors.white, size: 30.0),
                onTap: () async{
                  //redirect
                  await launch(NetworkUtil.PERSYARATAN_PUSAT_URL);
                },
              ),
            ),
          ),
        ]
      ),
    );

    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );

    return Scaffold(
      backgroundColor: DesignCourseAppTheme.grey,
      appBar: topAppBar,
      body: (_isShowing? Center(child: CircularProgressIndicator()):makeBody),
    );
  }
}

