import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elenpa/utils/network_util.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:elenpa/model/user.dart';
class newMessageScreen extends StatefulWidget {
  // ExamplePage({ Key key }) : super(key: key);
  @override
  newMessageScreenState createState() => new newMessageScreenState();
}

class newMessageScreenState extends State<newMessageScreen> {
  // final formKey = new GlobalKey<FormState>();
  // final key = new GlobalKey<ScaffoldState>();
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  final TextEditingController _filter = new TextEditingController();
  String _searchText = "";
  List names = new List();
  List filteredNames = new List();
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = new Text( 'Cari Kontak' );

  newMessageScreenState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredNames = names;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  @override
  void initState() {
    this._getNames();
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: (_isShowing? Center(child: CircularProgressIndicator()):Container(
        child: _buildList(),
      )),
      resizeToAvoidBottomPadding: false,
    );
  }

  Future<void> writeMessage(String id_user, String user_name) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('id_user_receiver', id_user);
    prefs.setString('user_name_receiver', user_name);
    Navigator.pushNamed(
      context, '/ChatScreen'
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: _appBarTitle,
      leading: new IconButton(
        icon: _searchIcon,
        onPressed: _searchPressed,

      ),
    );
  }

  Widget _buildList() {
    if (!(_searchText.isEmpty)) {
      List tempList = new List();
      for (int i = 0; i < filteredNames.length; i++) {
        print(filteredNames[i].nama_lengkap);
        if (filteredNames[i].nama_lengkap.toLowerCase().contains(_searchText.toLowerCase())) {
          tempList.add(filteredNames[i]);
        }
      }
      setState(() {
        filteredNames = tempList;
      });
    }
    return ListView.builder(
      itemCount: names == null ? 0 : filteredNames.length,
      itemBuilder: (BuildContext context, int index) {
        return new ListTile(
          title: Text(filteredNames[index].nama_lengkap),
          onTap: () => writeMessage(filteredNames[index].user_id,filteredNames[index].nama_lengkap),
        );
      },
    );
  }

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search),
              hintText: 'Cari...'
          ),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = new Text( 'Cari Kontak' );
        filteredNames = names;
        _filter.clear();
      }
    });
  }

  void _getNames() async {
    show();
    NetworkUtil _netUtil = new NetworkUtil();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    _netUtil.get(NetworkUtil.USER_URL,headers).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        return null;
      } else {
        setState(() {
          names = res['data'].map((model) => User.fromJson(model)).toList();
          filteredNames = res['data'].map((model) => User.fromJson(model)).toList();
        });
        dismiss();
      }
    });
  }


}