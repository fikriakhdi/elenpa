import 'package:elenpa/screen/homeScreen.dart';
import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import 'package:elenpa/theme/theme.dart';
import 'package:elenpa/model/user.dart';
import 'dart:async';
import 'package:http/http.dart' show Client;
import 'package:elenpa/utils/network_util.dart';
import 'package:elenpa/model/user.dart';
import 'package:elenpa/model/session.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditProfileScreen extends StatefulWidget {
//  ProfileScreen({Key key, this.title}) : super(key: key);

  final String title ="Edit Profile";

  @override
  _EditProfileScreenState createState() => new _EditProfileScreenState();
}
final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
class _EditProfileScreenState extends State<EditProfileScreen> {
  NetworkUtil _netUtil = new NetworkUtil();
  bool _isShowing = false;
  // method do change state and show our CircularProgressBar
  void show() {
    setState(() => _isShowing = true);
  }

  // method to change state and hide our CIrcularProgressBar
  void dismiss() {
    setState(() => _isShowing = false);
  }
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  TextEditingController nama_lengkap = new TextEditingController();
  TextEditingController alamat = new TextEditingController();
  TextEditingController notelp = new TextEditingController();
  TextEditingController email = new TextEditingController();
  int _selectedProvince = null;
  int _selectedCity = null;
  dynamic User = {"nama_lengkap":"", "email":""};
  List<DropdownMenuItem<int>> provinceList = [];
  List<DropdownMenuItem<int>> cityList = [];

  Future<void> loadProvinceList() async{
    show();
    provinceList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    return _netUtil.get(NetworkUtil.PROVINCE_URL,headers).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        dismiss();
        return null;
      } else {
        setState(() {
          print(res['data']);
          res['data'].map((model) => provinceList.add(new DropdownMenuItem(
            child: new Text(model['nama_provinsi']),
            value: int.parse((model['provinsi_id']!=null?model['provinsi_id']:0)),
          ))).toList();
        });
        dismiss();
      }
    });
  }

  Future<void> loadCityList(String provinsi_id) async{
//    show();
    cityList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"provinsi_id": provinsi_id};
    return _netUtil.post(NetworkUtil.CITY_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        dismiss();
        return null;
      } else {
        setState(() {
          print(res['data']);
          res['data'].map((model) => cityList.add(new DropdownMenuItem(
            child: new Text(model['nama_kota']),
            value: int.parse(model['kota_id']),
          ))).toList();
          _selectedCity = int.parse(res['data'][0]['kota_id']);
        });
        dismiss();
      }
    });
  }

  Future<void> updateProfile(String nama_lengkap, email, alamat, nomor_telp,provinsi_id, kota_id) async{
    show();
    cityList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"nama_lengkap": nama_lengkap,"email":email, "provinsi_id":provinsi_id, "kota_id":kota_id, "alamat":alamat, "nomor_telp":nomor_telp};
    return _netUtil.post(NetworkUtil.UPDATE_PROFILE_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        dismiss();
        return null;
      } else {
        getProfileData();
        dismiss();
        return true;
      }
    });
  }

  Future<void> setCityList(String provinsi_id) async{
    show();
    cityList = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    NetworkUtil _netUtil = new NetworkUtil();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    Map<String, String> body = {"provinsi_id": provinsi_id};
    return _netUtil.post(NetworkUtil.CITY_URL,headers:headers, body:body).then((dynamic res) async{
      if(res['status']==400) {
        print("Error to get profile data");
        dismiss();
        return null;
      } else {
        setState(() {
          res['data'].map((model) => cityList.add(new DropdownMenuItem(
            child: new Text(model['nama_kota']),
            value: int.parse(model['kota_id']),
          ))).toList();
          _selectedCity = int.parse(res['data'][0]['kota_id']);
        });
        dismiss();
      }
    });
  }
  Future<bool> signout() async{
    show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    return _netUtil.get(NetworkUtil.LOGOUT_URL,headers).then((dynamic res) async{
      if(res['status']==400) {
        return false;
      }
      else {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("userId", "");
        prefs.setString("P3NPA_PIN", "");
        dismiss();
        return true;
      }
    });
  }

  Future<void> getProfileData() async{

    show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Token = prefs.getString('token');
    Map<String, String> headers = {"token": Token};
    return _netUtil.get(NetworkUtil.PROFILE_URL, headers).then((
        dynamic res) async {
      if (res['status'] == 400) {
        print("Error to get profile data");
      } else{
        setState(() {
          User = res['data'];
          nama_lengkap.text = User['nama_lengkap'];
          alamat.text = User['alamat'];
          notelp.text = User['nomor_telp'];
          email.text = User['email'];
          _selectedProvince = int.parse(User['provinsi_id']);
          setCityList(User['provinsi_id']).then((ret){
            _selectedCity = int.parse(User['kota_id']);
          });
        });
        dismiss();
      }
    });
  }
  @override
  void initState() {
    show();
    getProfileData();
    loadProvinceList();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {



    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: DesignCourseAppTheme.grey,
      title: Text(widget.title),
    );
    return new Scaffold(
        key: _scaffoldState,
        appBar: topAppBar,
        body: (_isShowing? Center(child: CircularProgressIndicator()):new ListView(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: <Widget>[
                    new Container(
                        width: 100.0,
                        height: 100.0,
                        child: FadeInImage.assetNetwork(
                            placeholder: 'assets/images/user.png',
                            image:NetworkUtil.ASSETS_FILE_URL+User['photo_user']
                        )),
                    SizedBox(height: 15.0),
                    TextFormField(
                      controller:nama_lengkap,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Tolong Masukkan Nama lengkap Anda';
                        }
                        return null;
                      },

                      decoration: new InputDecoration(hintText: "Nama Lengkap", contentPadding: const EdgeInsets.all(20.0)),
                    ),
                    TextFormField(
                      controller:email,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Tolong Masukkan Email Anda';
                        }
                        return null;
                      },

                      decoration: new InputDecoration(hintText: "Email", contentPadding: const EdgeInsets.all(20.0)),
                    ),
                    TextFormField(
                      controller:notelp,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Tolong Masukkan No. Telp Anda';
                        }
                        return null;
                      },

                      decoration: new InputDecoration(hintText: "Nomor Telepon", contentPadding: const EdgeInsets.all(20.0)),
                    ),
                    TextFormField(
                      controller:alamat,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Tolong Masukkan Alamat Anda';
                        }
                        return null;
                      },

                      decoration: new InputDecoration(hintText: "Alamat", contentPadding: const EdgeInsets.all(20.0)),
                    ),
                    SizedBox(height: 15.0),
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: DropdownButton(
                      hint: new Text('Pilih Provinsi'),
                      items: provinceList,
                      value: _selectedProvince,
                      onChanged: (value) {
                        setState(() {
                          _selectedProvince = value;
                          loadCityList(value.toString());
                        });
                      },
                      isExpanded: true,
                    ),

                    ),
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: DropdownButton(
                        hint: new Text('Pilih Kota'),
                        items: cityList,
                        value: _selectedCity,
                        onChanged: (value) {
                          setState(() {
                            _selectedCity = value;
                          });
                        },
                        isExpanded: true,
                      ),

                    ),
                    Container(
                        height: 45.0,
                        width: 115.0,
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          shadowColor: Colors.greenAccent,
                          color: Colors.green,
                          elevation: 7.0,
                          child: GestureDetector(
                            onTap: () {updateProfile(nama_lengkap.text, email.text, alamat.text, notelp.text, _selectedProvince.toString(), _selectedCity.toString()).then((isSuccess) {

                              _scaffoldState.currentState.showSnackBar(
                                SnackBar(
                                  content: Text("Update Success"),
                                ),
                              );
                            });},
                            child: Center(
                              child: Text(
                                'SIMPAN',
                                style: TextStyle(color: Colors.white,
                                    fontSize: 15.0,
                                    fontFamily: 'Montserrat'),
                              ),
                            ),
                          ),
                        )),
                  ],
                ))
          ],
        )));
  }

}


class getClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();

    path.lineTo(0.0, size.height / 1.9);
    path.lineTo(size.width + 125, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}