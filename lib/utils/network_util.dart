import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:convert';

class NetworkUtil {
  //List Of URL
  static final BASE_URL = "http://sipatnarima.sda.jabarprov.go.id/";
  static final API_URL = BASE_URL+"api";
  static final LOGIN_URL = API_URL + "/signin";
  static final PROFILE_URL = API_URL + "/profile";
  static final LOGOUT_URL = API_URL + "/signout";
  static final DOWNLOAD_URL = API_URL + "/download";
  static final INTAKE_URL = API_URL + "/intake";
  static final INTAKE_SINGLE_URL = API_URL + "/intake_single";
  static final INTAKE_COMPANY_URL = API_URL + "/intake_company";
  static final COMPANY_URL = API_URL + "/company";
  static final USAGE_URL = API_URL + "/usage_all";
  static final USAGE_SINGLE_URL = API_URL + "/usage_single/";
  static final SLIDER_URL = API_URL + "/slider";
  static final MESSAGE_URL = API_URL + "/message";
  static final SET_USAGE_URL = API_URL + "/usage";
  static final ASSETS_FILE_URL = BASE_URL+"assets/uploadfiles/";
  static final SIPPA_URL = API_URL + "/sippa";
  static final SIPPA_SINGLE_URL = API_URL + "/sippa_single";
  static final CREATE_PIN_URL = API_URL + "/create_pin";
  static final CHECK_PIN_URL = API_URL + "/check_pin";
  static final USER_URL = API_URL + "/user";
  static final CHAT_URL = API_URL + "/chat";
  static final SEND_CHAT_URL = API_URL + "/send_chat";
  static final PROVINCE_URL = API_URL + "/province";
  static final CITY_URL = API_URL + "/city";
  static final BANNER_URL = API_URL + "/banner";
  static final UPDATE_PROFILE_URL = API_URL + "/update_profile";
  static final PERUSAHAAN_URL = API_URL + "/perusahaan";
  static final PAJAK_BULAN_LALU_URL = API_URL + "/pajak_bulan_lalu";
  static final PAJAK_BULAN_INI_URL = API_URL + "/pajak_bulan_ini";
  static final INPUT_VOLUME_URL = API_URL + "/input_volume";
  static final SET_TOKEN_FIREBASE_URL = API_URL + "/set_token_firebase";
  static final UPLOADED_SIPPA_URL = API_URL + "/uploaded_sippa";
  static final UPLOAD_SIPPA_URL = API_URL + "/upload_sippa";
  static final SINGLE_USER = API_URL + "/single_user";
  static final UPDATE_COMPANY_URL = API_URL + "/update_company";
  static final UPDATE_USAGE_URL = API_URL + "/update_usage";
  static final UPDATE_INTAKE_URL = API_URL + "/update_intake";
  static final KEWENANGAN_PROVINSI_JABAR_URL = "https://dpmptsp.jabarprov.go.id/sicantik/main/pendaftaranbaru";
  static final KEWENANGAN_PUSAT_URL = "http://rekomtek.pdsda.online/";
  static final PERSYARATAN_JABAR_URL = "https://dpmptsp.jabarprov.go.id/sicantik/main/jenis_perizinan/syarat/445";
  static final PERSYARATAN_PUSAT_URL = "https://dpmptsp.jabarprov.go.id/sicantik/main/jenis_perizinan/syarat/445";
  // next three lines makes this class a Singleton
  static NetworkUtil _instance = new NetworkUtil.internal();
  NetworkUtil.internal();
  factory NetworkUtil() => _instance;

  final JsonDecoder _decoder = new JsonDecoder();

  Future<dynamic> get(String url,Map headers) {
    return http.get(url,headers:headers).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

      final Decoded = jsonDecode(res);
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }
      return Decoded;
    });
  }

  Future<dynamic> post(String url, {Map headers, body, encoding}) {
    return http
        .post(url, body: body, headers: headers, encoding: encoding)
        .then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;
      final Decoded = jsonDecode(res);
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }
      return Decoded;
    });
  }
}