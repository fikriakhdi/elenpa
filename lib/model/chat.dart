import 'dart:convert';

Chat postFromJson(String str) {
  final jsonData = json.decode(str);
  return Chat.fromJson(jsonData);
}

String postToJson(Chat data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
class Chat {
  String receiver_name;
  String receiver_id;
  String receiver_photo;
  String sender_name;
  String sender_id;
  String sender_photo;
  String isi_pesan;
  String text;
  String read;
  String date;

  Chat(
      {this.receiver_name, this.receiver_photo, this.receiver_id, this.sender_name, this.sender_photo, this.sender_id,  this.isi_pesan, this.text, this.read, this.date});

  factory Chat.fromJson(Map<String, dynamic> json) => new Chat(
    receiver_id : json['receiver_id'].toString(),
    receiver_name: json["receiver_name"].toString(),
    receiver_photo: json["sender_name"].toString(),
    sender_photo: json["sender_photo"].toString(),
    sender_id: json["sender_id"].toString(),
    isi_pesan: json["isi_pesan"].toString(),
    text: json["text"].toString(),
    read: json["read"].toString(),
    date: json["date"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "receiver_id": receiver_id,
    "receiver_name": receiver_name,
    "receiver_photo": receiver_photo,
    "sender_photo": sender_photo,
    "sender_id": sender_id,
    "isi_pesan": isi_pesan,
    "text": text,
    "read": read,
    "date": date
  };
}

