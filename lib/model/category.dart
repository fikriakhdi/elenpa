class Category {
  String title;
  int lessonCount;
  int money;
  double rating;
  String imagePath;
  String screen;
  Category({
    this.title = '',
    this.imagePath = '',
    this.money = 0,
    this.rating = 0.0,
    this.screen = ''
  });



  static List<Category> MenuPerusahaan = [
    Category(
      imagePath: 'assets/images/4.png',
      title: 'SIPPA',
      money: 25,
      rating: 4.8,
      screen:'SippaScreen'
    ),
    Category(
      imagePath: 'assets/images/1.png',
      title: 'Rekap Pemakaian',
      money: 208,
      rating: 4.9,
      screen:'PemakaianScreen'
    ),
    Category(
      imagePath: 'assets/images/6.png',
      title: 'Input Volume',
      money: 25,
      rating: 4.8,
      screen:'inputVolumeMenuScreen'
    ),
    Category(
      imagePath: 'assets/images/10.png',
      title: 'Faktur Pajak',
      money: 208,
      rating: 4.9,
      screen:'TaxScreen'
    ),
    Category(
      imagePath: 'assets/images/7.png',
      title: 'Inbox',
      money: 208,
      rating: 4.9,
      screen:'PesanScreen'
    ),
    Category(
      imagePath: 'assets/images/9.png',
      title: 'Data Perusahaan',
      money: 208,
      rating: 4.9,
      screen:'CompanyScreen'
    ),
    Category(
      imagePath: 'assets/images/8.png',
      title: 'Download',
      money: 208,
      rating: 4.9,
      screen:'DownloadScreen'
    ),
    Category(
      imagePath: 'assets/images/3.png',
      title: 'Map',
      money: 208,
      rating: 4.9,
      screen:'MapScreen'
    ),
    Category(
      imagePath: 'assets/images/11.png',
      title: 'Laporan',
      money: 208,
      rating: 4.9,
      screen:'ReportScreen'
    ),
  ];

  static List<Category> MenuUptd = [
    Category(
        imagePath: 'assets/images/4.png',
        title: 'SIPPA',
        money: 25,
        rating: 4.8,
        screen:'SippaScreen'
    ),
    Category(
        imagePath: 'assets/images/12.png',
        title: 'Unggah SIPPA',
        money: 25,
        rating: 4.8,
        screen:'UploadSippaScreen'
    ),
    Category(
        imagePath: 'assets/images/1.png',
        title: 'Rekap Pemakaian',
        money: 208,
        rating: 4.9,
        screen:'PemakaianScreen'
    ),
    // Category(
    //     imagePath: 'assets/images/10.png',
    //     title: 'Input Faktur Pajak',
    //     money: 25,
    //     rating: 4.8,
    //     screen:'InputTaxScreen'
    // ),
    // Category(
    //     imagePath: 'assets/images/6.png',
    //     title: 'Berita Acara NPA',
    //     money: 208,
    //     rating: 4.9,
    //     screen:'BeritaAcaraScreen'
    // ),
    Category(
        imagePath: 'assets/images/7.png',
        title: 'Inbox',
        money: 208,
        rating: 4.9,
        screen:'PesanScreen'
    ),
    Category(
        imagePath: 'assets/images/9.png',
        title: 'Data Perusahaan',
        money: 208,
        rating: 4.9,
        screen:'CompanyScreen'
    ),
    Category(
        imagePath: 'assets/images/8.png',
        title: 'Download',
        money: 208,
        rating: 4.9,
        screen:'DownloadScreen'
    ),
    Category(
        imagePath: 'assets/images/3.png',
        title: 'Map',
        money: 208,
        rating: 4.9,
        screen:'MapScreen'
    ),
    Category(
        imagePath: 'assets/images/11.png',
        title: 'Laporan',
        money: 208,
        rating: 4.9,
        screen:'ReportScreen'
    ),
  ];
}
