import 'dart:convert';

BannerPict postFromJson(String str) {
  final jsonData = json.decode(str);
  return BannerPict.fromJson(jsonData);
}

String postToJson(BannerPict data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
class BannerPict {
  String banner_id;
  String file_url;
  String tanggal;

  BannerPict(
      {this.banner_id, this.file_url, this.tanggal});

  factory BannerPict.fromJson(Map<String, dynamic> json) => new BannerPict(
    banner_id: json["banner_id"],
    file_url: json["file_url"],
    tanggal: json["tanggal"],
  );

  Map<String, dynamic> toJson() => {
    "banner_id": banner_id,
    "file_url": file_url,
    "tanggal": tanggal,
  };
}

