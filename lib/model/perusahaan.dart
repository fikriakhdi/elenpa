import 'dart:convert';

Perusahaan postFromJson(String str) {
  final jsonData = json.decode(str);
  return Perusahaan.fromJson(jsonData);
}

String postToJson(Perusahaan data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
class Perusahaan {
  String id_perusahaan;
  String nama_perusahaan;
  String nama_provinsi;
  String nama_kota;
  String nama_pppd;

  Perusahaan(
      {this.nama_perusahaan, this.nama_provinsi, this.nama_kota, this.nama_pppd, this.id_perusahaan});

  factory Perusahaan.fromJson(Map<String, dynamic> json) => new Perusahaan(
    id_perusahaan: json["id_perusahaan"],
    nama_perusahaan: json["nama_perusahaan"],
    nama_provinsi: json["nama_provinsi"],
    nama_kota: json["nama_kota"],
    nama_pppd: json["nama_pppd"]
  );

  Map<String, dynamic> toJson() => {
    "id_perusahaan": id_perusahaan,
    "nama_perusahaan": nama_perusahaan,
    "nama_provinsi": nama_provinsi,
    "nama_kota": nama_kota,
    "nama_pppd": nama_pppd
  };
}

