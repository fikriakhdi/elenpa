import 'dart:convert';

User_Pin postFromJson(String str) {
  final jsonData = json.decode(str);
  return User_Pin.fromJson(jsonData);
}

String postToJson(User_Pin data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class User_Pin {
  String pin_id;
  String user_id;
  String pin;
  String imei;

  User_Pin(
      {this.pin_id, this.user_id, this.pin, this.imei});

  factory User_Pin.fromJson(Map<String, dynamic> json) => new User_Pin(
      pin_id: json["pin_id"],
      user_id: json["user_id"],
      pin: json["pin"],
      imei: json["imei"]
  );

  Map<String, dynamic> toJson() => {
    "pin_id": pin_id,
    "user_id": user_id,
    "pin": pin,
    "imei": imei
  };
}

