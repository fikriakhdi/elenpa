import 'dart:convert';

Colleges postFromJson(String str) {
  final jsonData = json.decode(str);
  return Colleges.fromJson(jsonData);
}

String postToJson(Colleges data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
class Colleges {
  String id;
  String name;
  String address;
  String type;
  String lat;
  String lng;

  Colleges(
      {this.id, this.name, this.address, this.type, this.lat, this.lng});

  factory Colleges.fromJson(Map<String, dynamic> json) => new Colleges(
    id: json["id"],
    name: json["name"],
    address: json["address"],
    type: json["type"],
    lat: json["lat"],
    lng: json["lng"]
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "address": address,
    "type": type,
    "lat": lat,
    "lng": lng
  };
}

