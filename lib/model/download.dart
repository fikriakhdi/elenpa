import 'dart:convert';

Download postFromJson(String str) {
  final jsonData = json.decode(str);
  return Download.fromJson(jsonData);
}

String postToJson(Download data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
class Download {
  String download_id;
  String keterangan;
  String upload_file;
  String tanggal_upload;

  Download(
      {this.download_id, this.keterangan, this.upload_file, this.tanggal_upload});

  factory Download.fromJson(Map<String, dynamic> json) => new Download(
    download_id: json["download_id"],
    keterangan: json["keterangan"],
    upload_file: json["upload_file"],
    tanggal_upload: json["tanggal_upload"],
  );

  Map<String, dynamic> toJson() => {
    "download_id": download_id,
    "keterangan": keterangan,
    "upload_file": upload_file,
    "tanggal_upload": tanggal_upload,
  };
}

