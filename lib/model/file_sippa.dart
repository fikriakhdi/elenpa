import 'dart:convert';

File_Sippa postFromJson(String str) {
  final jsonData = json.decode(str);
  return File_Sippa.fromJson(jsonData);
}

String postToJson(File_Sippa data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
class File_Sippa {
  String nama_perusahaan;
  String nama_dinas;
  String file_url;
  String tanggal;

  File_Sippa(
      {this.nama_perusahaan, this.nama_dinas, this.file_url, this.tanggal});

  factory File_Sippa.fromJson(Map<String, dynamic> json) => new File_Sippa(
      nama_perusahaan: json["nama_perusahaan"],
      nama_dinas: json["nama_dinas"],
      file_url: json["file_url"],
      tanggal: json["tanggal"]
  );

  Map<String, dynamic> toJson() => {
    "nama_perusahaan": nama_perusahaan,
    "nama_dinas": nama_dinas,
    "file_url": file_url,
    "tanggal": tanggal
  };
}

