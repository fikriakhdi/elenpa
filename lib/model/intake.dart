import 'dart:convert';

Intake postFromJson(String str) {
  final jsonData = json.decode(str);
  return Intake.fromJson(jsonData);
}

String postToJson(Intake data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
class Intake {
  String intake_id;
  String perusahaan_id;
  String cppd_id;
  String nomor_sippa_awal;
  String sumber_mata_air;
  String tanggal_penetapan;
  String lat;
  String lng;

  Intake(
      {this.intake_id, this.perusahaan_id, this.cppd_id, this.nomor_sippa_awal, this.sumber_mata_air, this.tanggal_penetapan, this.lat, this.lng});

  factory Intake.fromJson(Map<String, dynamic> json) => new Intake(
      intake_id: json["intake_id"],
      perusahaan_id: json["perusahaan_id"],
      cppd_id: json["cppd_id"],
      nomor_sippa_awal: json["nomor_sippa_awal"],
      sumber_mata_air: json["sumber_mata_air"],
      tanggal_penetapan: json["tanggal_penetapan"],
      lat: json["lat"],
      lng: json["lng"],
  );

  Map<String, dynamic> toJson() => {
    "intake_id": intake_id,
    "perusahaan_id": perusahaan_id,
    "cppd_id": cppd_id,
    "nomor_sippa_awal": nomor_sippa_awal,
    "sumber_mata_air": sumber_mata_air,
    "tanggal_penetapan": tanggal_penetapan,
    "lat": lat,
    "lng": lng
  };
}

