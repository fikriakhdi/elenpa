class Session {
  String _token;
  String _user_id;
  Session( this._token, this._user_id);

  Session.map(dynamic obj) {
    this._token = obj["token"];
    this._user_id = obj["user_id"];
  }

  String get token => _token;
  String get user_id => _user_id;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["token"] = _token;
    map["user_id"] = _user_id;

    return map;
  }
}