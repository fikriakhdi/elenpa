import 'dart:convert';

Sippa postFromJson(String str) {
  final jsonData = json.decode(str);
  return Sippa.fromJson(jsonData);
}

String postToJson(Sippa data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
class Sippa {
  String id;
  String nomor_sippa;
  String nomor_sippa_awal;
  String sumber_mata_air;
  String tanggal_penetapan;
  String tanggal_habis;
  String status;
  String water_meter;
  String max_debit;

  Sippa(
      {this.id,this.nomor_sippa, this.sumber_mata_air, this.nomor_sippa_awal, this.tanggal_penetapan, this.tanggal_habis, this.status, this.water_meter, this.max_debit});

  factory Sippa.fromJson(Map<String, dynamic> json) => new Sippa(
      id: json["id"],
      nomor_sippa: json["nomor_sippa"],
      sumber_mata_air: json["sumber_mata_air"],
      nomor_sippa_awal: json["nomor_sippa_awal"],
      tanggal_penetapan: json["tanggal_penetapan"],
      tanggal_habis: json["tanggal_habis"],
      status: json["status"],
      water_meter: json["water_meter"],
      max_debit: json["max_debit"]

  );

  Map<String, dynamic> toJson() => {
    "id":id,
    "nomor_sippa": nomor_sippa,
    "sumber_mata_air": sumber_mata_air,
    "nomor_sippa_awal": nomor_sippa_awal,
    "tanggal_penetapan": tanggal_penetapan,
    "tanggal_habis": tanggal_habis,
    "status": status,
    "water_meter": water_meter,
    "max_debit": max_debit

  };
}

