import 'dart:convert';

Pesan postFromJson(String str) {
  final jsonData = json.decode(str);
  return Pesan.fromJson(jsonData);
}

String postToJson(Pesan data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
class Pesan {
  String nama_lawan;
  String foto_lawan;
  String isi_pesan;
  String balas;
  String penerima;
  String tanggal_kirim;

  Pesan(
      {this.nama_lawan,this.penerima, this.foto_lawan, this.isi_pesan, this.balas, this.tanggal_kirim});

  factory Pesan.fromJson(Map<String, dynamic> json) => new Pesan(
    penerima: json["penerima"].toString(),
    nama_lawan: json["nama_lawan"].toString(),
      foto_lawan: json["foto_lawan"].toString(),
      isi_pesan: json["isi_pesan"].toString(),
    balas: json["balas"].toString(),
    tanggal_kirim: json["tanggal_kirim"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "penerima": penerima,
    "nama_lawan": nama_lawan,
    "foto_lawan": foto_lawan,
    "isi_pesan": isi_pesan,
    "balas": balas,
    "tanggal_kirim": tanggal_kirim,
  };
}

