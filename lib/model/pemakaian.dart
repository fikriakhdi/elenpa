import 'dart:convert';

Pemakaian postFromJson(String str) {
  final jsonData = json.decode(str);
  return Pemakaian.fromJson(jsonData);
}

String postToJson(Pemakaian data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}
class Pemakaian {
  String id;
  String nomor_sippa;
  String nama_lengkap;
  String nama_pppd;
  String sumber_mata_air;
  String volume_meter;
  String npa;
  String pajak;
  String tanggal;

  Pemakaian(
      {this.id, this.nomor_sippa,this.nama_lengkap, this.nama_pppd, this.sumber_mata_air, this.volume_meter, this.npa, this.tanggal, this.pajak});

  factory Pemakaian.fromJson(Map<String, dynamic> json) => new Pemakaian(

    id:json['id'],
    nomor_sippa: json["nomor_sippa"],
    nama_lengkap: json["nama_lengkap"],
    nama_pppd: json["nama_pppd"],
    sumber_mata_air: json["sumber_mata_air"],
    volume_meter: json["volume_meter"],
    npa: json["npa"],
    pajak: json["pajak"],
    tanggal: json["tanggal"],
  );

  Map<String, dynamic> toJson() => {
    "id":id,
    "nomor_sippa":nomor_sippa,
    "nama_lengkap": nama_lengkap,
    "nama_pppd": nama_pppd,
    "sumber_mata_air": sumber_mata_air,
    "volume_meter": volume_meter,
    "npa": npa,
    "pajak": pajak,
    "tanggal": tanggal,
  };
}

