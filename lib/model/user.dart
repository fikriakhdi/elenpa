
import 'dart:convert';

User postFromJson(String str) {
  final jsonData = json.decode(str);
  return User.fromJson(jsonData);
}

String postToJson(User data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class User {
  String user_id;
  String nama_lengkap;
  String username;
  String password;
  String provinsi_id;
  String kota_id;
  String alamat;
  String nomor_telp;
  String email;
  String user_roles;
  String photo_user;
  String user_status;
  String last_login;

  User({
    this.user_id,
    this.nama_lengkap,
    this.username,
    this.password,
    this.provinsi_id,
    this.kota_id,
    this.alamat,
    this.nomor_telp,
    this.email,
    this.user_roles,
    this.photo_user,
    this.user_status,
    this.last_login,
  });

  factory User.fromJson(Map<String, dynamic> json) => new User(
    user_id: json["user_id"],
    nama_lengkap: json["nama_lengkap"],
    username: json["username"],
    password: json["password"],
    provinsi_id: json["provinsi_id"],
    kota_id: json["kota_id"],
    alamat: json["alamat"],
    nomor_telp: json["nomor_telp"],
    email: json["email"],
    user_roles: json["user_roles"],
    photo_user: json["photo_user"],
    user_status: json["user_status"],
    last_login: json["last_login"],
  );

  Map<String, dynamic> toJson() => {
    "user_id": user_id,
    "nama_lengkap": nama_lengkap,
    "username": username,
    "password": password,
    "provinsi_id": provinsi_id,
    "kota_id": kota_id,
    "alamat": alamat,
    "nomor_telp": nomor_telp,
    "email": email,
    "user_roles": user_roles,
    "photo_user": photo_user,
    "user_status": user_status,
    "last_login": last_login,
  };
}