import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import screens
import 'package:elenpa/screen/inputVolumeScreen.dart';
import 'package:elenpa/screen/inputVolumeManualScreen.dart';
import 'package:elenpa/screen/splashScreen.dart';
import 'package:elenpa/screen/loginScreen.dart';
import 'package:elenpa/screen/homeScreen.dart';
import 'package:elenpa/screen/profileScreen.dart';
import 'package:elenpa/screen/editProfileScreen.dart';
import 'package:elenpa/screen/downloadScreen.dart';
import 'package:elenpa/screen/mapScreen.dart';
import 'package:elenpa/screen/pemakaianScreen.dart';
import 'package:elenpa/screen/sippaScreen.dart';
import 'package:elenpa/screen/pesanScreen.dart';
import 'package:elenpa/screen/companyScreen.dart';
import 'package:elenpa/screen/passcodeScreen.dart';
import 'package:elenpa/screen/createPassCodeScreen.dart';
import 'package:elenpa/screen/directScreen.dart';
import 'package:elenpa/screen/newMessageScreen.dart';
import 'package:elenpa/screen/chatScreen.dart';
import 'package:elenpa/screen/reportScreen.dart';
import 'package:elenpa/screen/pajakBulanIniScreen.dart';
import 'package:elenpa/screen/pajakBulanLaluScreen.dart';
import 'package:elenpa/screen/ocrScreen.dart';
import 'package:elenpa/screen/renewSippaScreen.dart';
import 'package:elenpa/screen/uploadSippaScreen.dart';
import 'package:elenpa/screen/newUploadSippaScreen.dart';
import 'package:elenpa/screen/firstUploadSippaScreen.dart';
import 'package:elenpa/screen/inputVolumeMenuScreen.dart';
import 'package:elenpa/screen/editPerusahaanScreen.dart';
import 'package:elenpa/screen/editPemakaianScreen.dart';
import 'package:elenpa/screen/detailPemakaianScreen.dart';
import 'package:elenpa/screen/detailPerusahaanScreen.dart';
import 'package:elenpa/screen/detailSippaScreen.dart';
import 'package:elenpa/screen/mapMenuScreen.dart';

void main() {
  SystemChrome.setPreferredOrientations(
          [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown])
      .then((_) => runApp(new MyApp()));
   runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness:
          Platform.isAndroid ? Brightness.dark : Brightness.light,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.dark,
    ));
    return new MaterialApp(
      home: new SplashScreen(),
      routes: <String, WidgetBuilder>{
        '/LoginScreen': (BuildContext context) => new LoginScreen(),
        '/HomeScreen': (BuildContext context) => new HomeScreen(),
        '/ProfileScreen': (BuildContext context) => new ProfileScreen(),
        '/EditProfileScreen': (BuildContext context) => new EditProfileScreen(),
        '/DownloadScreen': (BuildContext context) => new DownloadScreen(),
        '/MapScreen': (BuildContext context) => new MapScreen(),
        '/PemakaianScreen': (BuildContext context) => new PemakaianScreen(),
        '/SippaScreen': (BuildContext context) => new SippaScreen(),
        '/PesanScreen': (BuildContext context) => new PesanScreen(),
        '/CompanyScreen': (BuildContext context) => new CompanyScreen(),
        '/PasscodeScreen': (BuildContext context) => new passcodeScreen(),
        '/CreatePassCodeScreen': (BuildContext context) => new CreatePassCodeScreen(),
        '/DirectScreen': (BuildContext context) => new DirectScreen(),
        '/NewMessageScreen': (BuildContext context) => new newMessageScreen(),
        '/ChatScreen': (BuildContext context) => new ChatScreen(),
        '/ReportScreen': (BuildContext context) => new ReportScreen(),
        '/PajakBulanIniScreen': (BuildContext context) => new PajakBulanIniScreen(),
        '/PajakBulanLaluScreen': (BuildContext context) => new PajakBulanLaluScreen(),
        '/InputVolumeScreen': (BuildContext context) => new InputVolumeScreen(),
        '/InputVolumeManualScreen': (BuildContext context) => new InputVolumeManualScreen(),
        '/RenewSippaScreen': (BuildContext context) => new RenewSippaScreen(),
        '/UploadSippaScreen': (BuildContext context) => new UploadSippaScreen(),
        '/NewUploadSippaScreen': (BuildContext context) => new NewUploadSippaScreen(),
        '/FirstUploadSippaScreen': (BuildContext context) => new FirstUploadSippaScreen(),
        '/OcrScreen': (BuildContext context) => new OcrScreen(),
        '/inputVolumeMenuScreen': (BuildContext context) => new InputVolumeMenuScreen(),
        '/editPerusahaanScreen': (BuildContext context) => new EditPerusahaanScreen(),
        '/editPemakaianScreen': (BuildContext context) => new EditPemakaianScreen(),
        '/detailPemakaianScreen': (BuildContext context) => new DetailPemakaianScreen(),
        '/detailPerusahaanScreen': (BuildContext context) => new DetailPerusahaanScreen(),
        '/detailSippaScreen': (BuildContext context) => new DetailSippaScreen(),
        '/MapMenuScreen': (BuildContext context) => new MapMenuScreen(),
      },
    );
  }
}


class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
